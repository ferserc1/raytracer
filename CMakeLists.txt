
cmake_minimum_required (VERSION 2.6)
project(raytracer)

set(CMAKE_CXX_STANDARD 14)
find_package (Threads)

set(HEADER_DIR "${PROJECT_SOURCE_DIR}/include")
set(SOURCE_DIR "${PROJECT_SOURCE_DIR}/src")

include_directories("${HEADER_DIR}")

set(SOURCE_FILES "${SOURCE_DIR}/aabb.cpp"
				 "${SOURCE_DIR}/bvh_node.cpp"
				 "${SOURCE_DIR}/command_line.cpp"
				 "${SOURCE_DIR}/image.cpp"
				 "${SOURCE_DIR}/main.cpp"
				 "${SOURCE_DIR}/material.cpp"
				 "${SOURCE_DIR}/math_tools_imp.cpp"
				 "${SOURCE_DIR}/mesh.cpp"
				 "${SOURCE_DIR}/stb_image_imp.cpp"
				 "${SOURCE_DIR}/stb_image_write_imp.cpp")

add_definitions(-DBG2EMATH_PRECISION_DOUBLE)

add_executable(raytracer ${SOURCE_FILES})
target_link_libraries(raytracer ${CMAKE_THREAD_LIBS_INIT})
