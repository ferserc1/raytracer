#ifndef _RENDERER_HPP_
#define _RENDERER_HPP_

#include <functional>

class Renderer {
public:
	typedef std::function<void(int, int, int, int)> RenderFunction;

	Renderer();
	Renderer(bool mt);
	Renderer(bool mt, int bh, int bw);

	inline void setMultithreaded(bool mt) { _multithreaded = mt; }
	inline void setBuckets(int h, int v) { _bucketsHorizontal = h; _bucketsVertical = v; }

	// Render a frame of widthXheight pixels using RenderFunction, and returns the elapsed time
	float render(int width, int height, RenderFunction fn);

protected:
	bool _multithreaded = true;
	int _bucketsHorizontal = 20;
	int _bucketsVertical = 20;
};

#endif
