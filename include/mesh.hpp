#ifndef _MESH_HPP_
#define _MESH_HPP_

#include <hitable.hpp>
#include <aabb.hpp>

#include <vector>

using namespace bg::math;

class Mesh : public Hitable {
public:
	Mesh(const std::vector<real_t> & vert, const std::vector<real_t> & norm, const std::vector<uint32_t> & index, Material * mat);

	void setVertexArray(const std::vector<real_t> & vert);
	void setNormalArray(const std::vector<real_t> & norm);
	void setIndexArray(const std::vector<uint32_t> & index);

	virtual bool hit(const Ray & ray, real_t tmin, real_t tmax, HitRecord & rec) const;

	virtual bool boundingBox(float, float, AABB & box) const {
		box = _box;
		return true;
	}

protected:
	std::vector<Vector3> _v;
	std::vector<Vector3> _n;
	std::vector<uint32_t> _index;
	Material * _material;
	
	AABB _box;

	bool hitTriangle(uint32_t v0i, uint32_t v1i, uint32_t v2i, const Ray & ray, real_t tmin, real_t tmax, HitRecord & rec) const;
};

#endif
