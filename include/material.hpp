#ifndef _MATERIAL_H_
#define _MATERIAL_H_

#include <hitable.hpp>

#include <random>

extern bg::math::Vector3 randomInUnitSphere();
extern bg::math::Vector3 reflect(const bg::math::Vector3 & vector, const bg::math::Vector3 & normal);
extern bool refract(const bg::math::Vector3 & vec, const bg::math::Vector3 & normal, real_t ni_over_nt, bg::math::Vector3 & refracted);
extern real_t schlick(real_t cosine, real_t refractionIndex);

class Material {
public:
	virtual bool scatter(const Ray & ray, const HitRecord & rec, bg::math::Vector3 & attenuation, Ray & scattered) const = 0;
    virtual ~Material() {}
};

class AlbedoProperty {
public:
	AlbedoProperty(const bg::math::Vector3 & albedo) :_albedo(albedo) {}

	inline const bg::math::Vector3 & albedo() const { return _albedo; }
	inline bg::math::Vector3 & albedo() { return _albedo; }
	inline void setAlbedo(const bg::math::Vector3 & a) { _albedo = a; }

protected:
	bg::math::Vector3 _albedo;
};

class Lambertian : public Material, AlbedoProperty {
public:
	Lambertian(const bg::math::Vector3 & a) :AlbedoProperty(a) {}

	virtual bool scatter(const Ray & ray, const HitRecord & rec, bg::math::Vector3 & attenuation, Ray & scattered) const;
};

class Metal : public Material, AlbedoProperty {
public:
    Metal(const bg::math::Vector3 & a, real_t fuzz = 1.0) :AlbedoProperty(a), _fuzz(1.0) { if (fuzz<1.0) _fuzz = fuzz; }

	virtual bool scatter(const Ray & ray, const HitRecord & rec, bg::math::Vector3 & attenuation, Ray & scattered) const;
    
    inline real_t fuzz() const { return _fuzz; }
    inline void setFuzz(real_t f) { _fuzz = f; }
    
protected:
    real_t _fuzz;
};

class Dielectric : public Material {
public:
	Dielectric(real_t ri) : _refractionIndex(ri) {}

	virtual bool scatter(const Ray & ray, const HitRecord & rec, bg::math::Vector3 & attenuation, Ray & scattered) const;

	inline real_t refractionIndex() const { return _refractionIndex; }
	inline void setRefractionIndex(real_t ri) { _refractionIndex = ri; }

protected:
	real_t _refractionIndex;
};

#endif
