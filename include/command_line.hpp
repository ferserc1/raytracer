#ifndef _COMMAND_LINE_HPP_
#define _COMMAND_LINE_HPP_

#include <string>

class CommandLine {
public:
    CommandLine(int argc, const char ** argv);
    
    inline const std::string & outFile() const { return _outFile; }
    inline const std::string & inFile() const { return _inFile; }
    inline const std::string & inScene() const { return _inScene; }
    inline int width() const { return _width; }
    inline int height() const { return _height; }
    inline int samples() const { return _samples; }
    inline bool multithread() const { return _multithread; }
    inline int blurRadius() const { return _blurRadius; }

protected:
    std::string _outFile = "lightmap.jpg";
    std::string _inFile = "";
    std::string _inScene = "";
    int _width = 600;
    int _height = 600;
    int _samples = 5;
    bool _multithread = true;
    int _blurRadius = 0;
};

#endif
