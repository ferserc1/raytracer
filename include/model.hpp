#ifndef _MODEL_HPP_
#define _MODEL_HPP_

#include <math_tools.hpp>

#include <uv_map.hpp>

#include <vector>
#include <string>

struct PolyList {
	std::string name;

	std::vector<real_t> vertex;
	std::vector<real_t> normal;
	std::vector<uint32_t> index;
};

class Model {
public:
	bool load(const std::string & path, const bg::math::Matrix4 & transform = bg::math::Matrix4::Identity());

	void clear();

	inline const std::vector<PolyList*> & polyLists() const { return _polyLists; }

	inline const UVMap & lightmap() const { return _lightmap; }

protected:
	std::vector<PolyList*> _polyLists;

	UVMap _lightmap;
};

#endif
