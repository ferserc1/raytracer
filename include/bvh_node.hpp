#ifndef _BVH_NODE_HPP_
#define _BVH_NODE_HPP_

#include <hitable_list.hpp>
#include <aabb.hpp>

class BVHNode : public Hitable {
public:
	BVHNode();
	BVHNode(Hitable ** l, size_t listLength, float time0, float time1);

	virtual bool hit(const Ray & r, real_t tmin, real_t tmax, HitRecord & rec) const;
	virtual bool boundingBox(float t0, float ti, AABB & box) const;

protected:
	Hitable * _left;
	Hitable * _right;
	AABB _box;
};

#endif
