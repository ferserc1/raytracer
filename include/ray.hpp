#ifndef _RAY_HPP_
#define _RAY_HPP_

#include <math_tools.hpp>


class Ray {
public:
	Ray() {}
	Ray(const bg::math::Vector3 & origin, const bg::math::Vector3 & direction, real_t time = 0.0) :_origin(origin), _direction(direction), _time(time) { _direction.normalize(); }

	inline const bg::math::Vector3 & origin() const { return _origin; }
	inline bg::math::Vector3 & origin() { return _origin; }
	inline const bg::math::Vector3 & direction() const { return _direction; }
	inline bg::math::Vector3 & direction() { return _direction; }
	inline bg::math::Vector3 pointAtDistance(real_t t) const { return _origin + t * _direction; }
	inline real_t time() const { return _time; }
	inline void setTime(real_t t) { _time = t; }

protected:
	bg::math::Vector3 _origin;
	bg::math::Vector3 _direction;
	real_t _time;
};
#endif
