
#ifndef _MATH_TOOLS_H_
#define _MATH_TOOLS_H_

// Usage:
// Define BG2E_MATH_TOOLS_IMLEMENTATION and include this file in ONE cpp file to define the implementation
// Include this file in any other cpp file of your proyect

#define _USE_MATH_DEFINES
#ifdef _WIN32
#include <cmath>
#ifdef max
#undef max
#endif
#ifdef min
#undef min
#endif
#else
#include <math.h>
#endif

#include <cstdlib>
#include <cfloat>
#include <climits>
#include <string>
#include <sstream>
#include <limits>
#include <mutex>
#include <random>

#ifdef BG2EMATH_PRECISION_DOUBLE

typedef double real_t;

#else

typedef float real_t;

#endif

namespace bg {
namespace math {


extern const real_t kPi;
extern const real_t kPiOver2;
extern const real_t kPiOver4;
extern const real_t k2Pi;
extern const real_t kPiOver180;
extern const real_t k180OverPi;


namespace trigonometry {

inline real_t degreesToRadians(real_t d) {
	return d * kPiOver180;
}

inline real_t radiansToDegrees(real_t r) {
	return r * k180OverPi;
}

inline real_t sin(real_t f) {
#ifdef BG2EMATH_PRECISION_DOUBLE
	return static_cast<real_t>(::sin(f));
#else
	return static_cast<real_t>(sinf(f));
#endif
}

inline real_t cos(real_t f) {
#ifdef BG2EMATH_PRECISION_DOUBLE
	return static_cast<real_t>(::cos(f));
#else
	return static_cast<real_t>(cosf(f));
#endif
}

inline real_t tan(real_t f) {
#ifdef BG2EMATH_PRECISION_DOUBLE
	return static_cast<real_t>(::tan(f));
#else
	return static_cast<real_t>(tanf(f));
#endif
}

inline real_t asin(real_t f) {
#ifdef BG2EMATH_PRECISION_DOUBLE
	return static_cast<real_t>(::asin(f));
#else
	return static_cast<real_t>(asinf(f));
#endif
}

inline real_t acos(real_t f) {
#ifdef BG2EMATH_PRECISION_DOUBLE
	return static_cast<real_t>(::acos(f));
#else
	return static_cast<real_t>(acosf(f));
#endif
}

inline real_t cotan(real_t i) {
	// TODO: Fix this
	return static_cast<real_t>(1.0 / bg::math::trigonometry::tan(i));
}

inline real_t atan(real_t i) {
#ifdef BG2EMATH_PRECISION_DOUBLE
	return static_cast<real_t>(::atan(i));
#else
	return static_cast<real_t>(atanf(i));
#endif
}

inline real_t atan2(real_t i, real_t j) {
#ifdef BG2EMATH_PRECISION_DOUBLE
	return static_cast<real_t>(::atan2(i, j));
#else
	return static_cast<real_t>(atan2f(i, j));
#endif
}


extern const real_t deg;
extern const real_t rad;
}

namespace number {

inline short minValue(short val) {
	return SHRT_MIN;
}

inline short maxValue(short val) {
	return SHRT_MAX;
}

inline int minValue(int val) {
	return INT_MIN;
}

inline int maxValue(int val) {
	return INT_MAX;
}

inline long minValue(long val) {
	return LONG_MIN;
}

inline long maxValue(long val) {
	return LONG_MAX;
}

inline long long minValue(long long val) {
	return LLONG_MIN;
}

inline long long maxValue(long long val) {
	return LLONG_MAX;
}

inline unsigned short minValue(unsigned short val) {
	return 0;
}

inline unsigned short maxValue(unsigned short val) {
	return USHRT_MAX;
}

inline unsigned int minValue(unsigned int val) {
	return 0;
}

inline unsigned int maxValue(unsigned int val) {
	return UINT_MAX;
}

inline unsigned long minValue(unsigned long val) {
	return 0;
}

inline unsigned long maxValue(unsigned long val) {
	return ULONG_MAX;
}

inline unsigned long long minValue(unsigned long long val) {
	return 0;
}

inline unsigned long long maxValue(unsigned long long val) {
	return ULLONG_MAX;
}

inline float minValue(float val) {
	return -FLT_MAX;
}

inline float maxValue(float val) {
	return FLT_MAX;
}

inline double minValue(double val) {
	return -DBL_MAX;
}

inline double maxValue(double val) {
	return DBL_MAX;
}

template <class T> inline T clamp(T val, T min, T max) { return (val < min ? min : (val > max ? max : val)); }
}

inline float pow(float v, float i) {
	return ::powf(v, i);
}

inline double pow(double v, double i) {
	return ::pow(v, i);
}

inline double pow(double v, float i) {
	return ::pow(v, static_cast<double>(i));
}

inline float pow(float v, double i) {
	return ::powf(v, static_cast<float>(i));
}

inline double pow(double v, int i) {
	return ::pow(v, static_cast<double>(i));
}

inline float pow(float v, int i) {
	return ::powf(v, static_cast<float>(i));
}

inline float sqrt(float v) {
	return ::sqrt(v);
}

inline double sqrt(double v) {
	return ::sqrt(v);
}

inline short max(short a, short b) {
	return a>b ? a : b;
}

inline unsigned short max(unsigned short a, unsigned short b) {
	return a>b ? a : b;
}

inline int max(int a, int b) {
	return a>b ? a : b;
}

inline unsigned int max(unsigned int a, unsigned int b) {
	return a>b ? a : b;
}

inline long max(long a, long b) {
	return a>b ? a : b;
}

inline unsigned long max(unsigned long a, unsigned long b) {
	return a>b ? a : b;
}

inline long long max(long long a, long long b) {
	return a>b ? a : b;
}

inline unsigned long long max(unsigned long long a, unsigned long long b) {
	return a>b ? a : b;
}

inline float max(float a, float b) {
	return a>b ? a : b;
}

inline double max(double a, double b) {
	return a>b ? a : b;
}

inline short min(short a, short b) {
	return a>b ? b : a;
}

inline unsigned short min(unsigned short a, unsigned short b) {
	return a>b ? b : a;
}

inline int min(int a, int b) {
	return a>b ? b : a;
}

inline unsigned int min(unsigned int a, unsigned int b) {
	return a>b ? b : a;
}

inline long min(long a, long b) {
	return a>b ? b : a;
}

inline unsigned long min(unsigned long a, unsigned long b) {
	return a>b ? b : a;
}

inline long long min(long long a, long long b) {
	return a>b ? b : a;
}

inline unsigned long long min(unsigned long long a, unsigned long long b) {
	return a>b ? b : a;
}

inline float min(float a, float b) {
	return a>b ? b : a;
}

inline double min(double a, double b) {
	return a>b ? b : a;
}

inline short abs(short a) {
	return a<0 ? -a : a;
}

inline int abs(int a) {
	return a<0 ? -a : a;
}

inline long abs(long a) {
	return a<0L ? -a : a;
}

inline long long abs(long long a) {
	return a<0 ? -a : a;
}

inline float abs(float a) {
	return a<0.0f ? -a : a;
}

inline double abs(double a) {
	return a<0.0 ? -a : a;
}

inline float epsilon(float a) {
	return std::numeric_limits<float>::epsilon();
}

inline double epsilon(double a) {
	return std::numeric_limits<double>::epsilon();
}

inline real_t epsilon() {
#ifdef BG2EMATH_PRECISION_DOUBLE
	return std::numeric_limits<double>::epsilon();
#else
	return std::numeric_limits<float>::epsilon();
#endif
}

extern real_t random();

#ifdef BG2E_MATH_TOOLS_IMLEMENTATION
std::random_device rd;
std::mt19937 gen(rd());
std::uniform_real_distribution<> dis(0.0f, 1.0f);
std::mutex randomMutex;

real_t random() {
	std::lock_guard<std::mutex> l(randomMutex);
	return static_cast<real_t>(dis(gen));
}

#endif


inline real_t lerp(real_t from, real_t to, real_t t) {
	return (1.0f - t) * from + t * to;
}

inline bool isPowerOfTwo(int x) {
	return (x != 0 && (x & (~x + 1)) == x);
}

inline int nextPowerOfTwo(int x) {
	return static_cast<int>(::pow(2, ceil(log2(static_cast<double>(x)))));
}

inline float round(float n) {
	return roundf(n);
}

inline double round(double n) {
	return ::round(n);
}

namespace distance {

extern const real_t Km;
extern const real_t Hm;
extern const real_t Dam;
extern const real_t meter;
extern const real_t dm;
extern const real_t cm;
extern const real_t mm;
extern const real_t inch;
extern const real_t feet;
extern const real_t yard;
extern const real_t mile;

}

#ifdef BG2E_MATH_TOOLS_IMLEMENTATION


const real_t kPi = 3.14159265359f;
const real_t kPiOver180 = 0.01745329251994f;
const real_t k180OverPi = 57.29577951308233f;
const real_t kPiOver2 = 1.570796326794897f;
const real_t kPiOver4 = 0.785398163397448f;
const real_t k2Pi = 6.283185307179586f;

namespace trigonometry {
const real_t deg = k180OverPi;
const real_t rad = 1.0f;
}

namespace distance {

const real_t Km = 0.001f;
const real_t Hm = 0.01f;
const real_t Dam = 0.1f;
const real_t meter = 1.0f;
const real_t dm = 10.0f;
const real_t cm = 100.0f;
const real_t mm = 1000.0f;
const real_t inch = 39.37f;
const real_t feet = 3.28084f;
const real_t yard = 1.09361f;
const real_t mile = 0.000621371f;

}

#endif

template <class T>
class ScalarGeneric {
public:
	ScalarGeneric() :_value(0.0f) {}
	ScalarGeneric(T v, T factor = distance::meter) : _value(static_cast<T>(v) / factor) {}

	inline T value() const { return _value; }

	inline void setValue(T v, T factor = distance::meter) { _value = static_cast<T>(v) / factor; }

	inline ScalarGeneric<T> & operator =(const ScalarGeneric<T> & s) { _value = s._value; return *this; }
	inline bool operator ==(const ScalarGeneric<T> & s) { return _value == s._value; }

protected:
	T _value;
};

template <class T>
inline ScalarGeneric<T> operator + (const ScalarGeneric<T> & v1, const ScalarGeneric<T> & v2) {
	return ScalarGeneric<T>(v1.value() + v2.value());
}

template <class T>
inline ScalarGeneric<T> operator - (const ScalarGeneric<T> & v1, const ScalarGeneric<T> & v2) {
	return ScalarGeneric<T>(v1.value() - v2.value());
}

template <class T>
inline ScalarGeneric<T> operator * (const ScalarGeneric<T> & v1, const ScalarGeneric<T> & v2) {
	return ScalarGeneric<T>(v1.value() * v2.value());
}

template <class T>
inline ScalarGeneric<T> operator / (const ScalarGeneric<T> & v1, const ScalarGeneric<T> & v2) {
	return ScalarGeneric<T>(v1.value() / v2.value());
}

typedef ScalarGeneric<real_t> Scalarf;
typedef ScalarGeneric<double> Scalard;

#ifdef BG2EMATH_PRECISION_DOUBLE
typedef Scalard Scalar;
#else
typedef Scalarf Scalar;
#endif




template <class T>
class Vector2Generic {
public:
	Vector2Generic<T>() { _v[0] = static_cast<T>(0); _v[1] = static_cast<T>(0); }
	Vector2Generic<T>(T s) { _v[0] = s; _v[1] = s; }
	Vector2Generic<T>(T x, T y) { _v[0] = x; _v[1] = y; }
	Vector2Generic<T>(const T * v) { _v[0] = v[0]; _v[1] = v[1]; }
	Vector2Generic<T>(const Vector2Generic<T> & v) { _v[0] = v._v[0]; _v[1] = v._v[1]; }
	Vector2Generic<T>(const Scalar & s) { _v[0] = static_cast<T>(s.value()); _v[1] = static_cast<T>(s.value()); }
	Vector2Generic<T>(const Scalar & x, const Scalar & y) { _v[0] = static_cast<T>(x.value()); _v[1] = static_cast<T>(y.value()); }

	static Vector2Generic Lerp(const Vector2Generic<T> & from, const Vector2Generic<T> & to, real_t delta) {
		return Vector2Generic<T>(bg::math::lerp(from._v[0], to._v[0], delta),
			bg::math::lerp(from._v[1], to._v[1], delta));
	}

	inline T * raw() { return _v; }
	inline const T * raw() const { return _v; }
	inline int length() const { return 2; }

	inline real_t distance(const Vector2Generic<T> & v) const {
		Vector2Generic<T> v3(_v[0] - v._v[0],
			_v[1] - v._v[1]);
		return v3.magnitude();
	}

	inline real_t magnitude() const { return sqrtf(static_cast<real_t>(_v[0] * _v[0] + _v[1] * _v[1])); }
	inline real_t squaredMagnitude() const { return static_cast<real_t>(_v[0] * _v[0] + _v[1] * _v[1]); }
	inline Vector2Generic<T> & normalize() {
		T m = magnitude();
		_v[0] = _v[0] / m; _v[1] = _v[1] / m;
		return *this;
	}
	inline Vector2Generic<T> & add(const Vector2Generic<T> & v2) {
		_v[0] += v2._v[0];
		_v[1] += v2._v[1];
		return *this;
	}
	inline Vector2Generic<T> & sub(const Vector2Generic<T> & v2) {
		_v[0] -= v2._v[0];
		_v[1] -= v2._v[1];
		return *this;
	}
	inline T dot(const Vector2Generic<T> & v2) const {
		return _v[0] * v2._v[0] + _v[1] * v2._v[1];
	}
	inline Vector2Generic<T> & scale(T scale) {
		_v[0] *= scale; _v[1] *= scale;
		return *this;
	}
	inline Vector2Generic<T> & scale(const Scalar & s) {
		_v[0] *= s.value(); _v[1] *= s.value();
		return *this;
	}

	inline T operator[](int i) const { return _v[i]; }
	inline T & operator[](int i) { return _v[i]; }
	inline bool operator==(const Vector2Generic<T> & v) const { return _v[0] == v._v[0] && _v[1] == v._v[1]; }
	inline bool operator<(const Vector2Generic<T> & v) const { return _v[0] < v._v[0] && _v[1] < v._v[1]; }
	inline bool operator>(const Vector2Generic<T> & v) const { return _v[0] > v._v[0] && _v[1] > v._v[1]; }
	inline bool operator<=(const Vector2Generic<T> & v) const { return _v[0] <= v._v[0] && _v[1] <= v._v[1]; }
	inline bool operator>=(const Vector2Generic<T> & v) const { return _v[0] >= v._v[0] && _v[1] >= v._v[1]; }
	inline bool operator!=(const Vector2Generic<T> & v) const { return _v[0] != v._v[0] || _v[1] != v._v[1]; }
	inline void operator=(const Vector2Generic & v) { _v[0] = v._v[0]; _v[1] = v._v[1]; }
	inline void operator=(const T * v) { _v[0] = v[0]; _v[1] = v[1]; }

	inline Vector2Generic<T> & operator +=(const Vector2Generic<T> & v) { _v[0] += v->_v[0]; _v[1] += v->_v[1]; return *this; }
	inline Vector2Generic<T> & operator -=(const Vector2Generic<T> & v) { _v[0] -= v->_v[0]; _v[1] -= v->_v[1]; return *this; }
	inline Vector2Generic<T> & operator *=(const Vector2Generic<T> & v) { _v[0] *= v->_v[0]; _v[1] *= v->_v[1]; return *this; }
	inline Vector2Generic<T> & operator /=(const Vector2Generic<T> & v) { _v[0] /= v->_v[0]; _v[1] /= v->_v[1]; return *this; }
	inline Vector2Generic<T> & operator *=(const T v) { _v[0] *= v; _v[1] *= v; return *this; }
	inline Vector2Generic<T> & operator /=(const T v) { T k = static_cast<T>(1) / static_cast<T>(v); _v[0] *= k; _v[1] *= k; return *this; }

	inline std::string toString() const {
		std::stringstream ss;
		ss << '[' << _v[0] << "," << _v[1] << ']';
		return ss.str();
	}

	inline void set(T v) { _v[0] = v; _v[1] = v; }
	inline void set(T x, T y) { _v[0] = x; _v[1] = y; }
	inline void set(const Scalar & v) { _v[0] = static_cast<T>(v.value()); _v[1] = static_cast<T>(v.value()); }
	inline void set(const Scalar & x, const Scalar & y) { _v[0] = static_cast<T>(x.value()); _v[1] = static_cast<T>(y.value()); }

	inline T x() const { return _v[0]; }
	inline T y() const { return _v[1]; }

	inline void x(T v) { _v[0] = v; }
	inline void y(T v) { _v[1] = v; }
	inline void x(const Scalar & v) { _v[0] = static_cast<T>(v.value()); }
	inline void y(const Scalar & v) { _v[1] = static_cast<T>(v.value()); }

	inline T width() const { return _v[0]; }
	inline T height() const { return _v[1]; }

	inline void width(T v) { _v[0] = v; }
	inline void height(T v) { _v[1] = v; }
	inline void width(const Scalar & v) { _v[0] = static_cast<T>(v.value()); }
	inline void height(const Scalar & v) { _v[1] = static_cast<T>(v.value()); }

	inline T aspectRatio() const { return static_cast<T>(_v[0]) / static_cast<T>(_v[1]); }

	inline Vector2Generic<T> & clamp(T min, T max) {
		_v[0] = number::clamp<T>(_v[0], min, max);
		_v[1] = number::clamp<T>(_v[1], min, max);
		return *this;
	}
	inline Vector2Generic<T> & clamp(const Scalar & min, const Scalar & max) {
		_v[0] = number::clamp<T>(_v[0], static_cast<T>(min.value()), static_cast<T>(max.value()));
		_v[1] = number::clamp<T>(_v[1], static_cast<T>(min.value()), static_cast<T>(max.value()));
		return *this;
	}

	inline bool isNan() {
		return isnan(_v[0]) || isnan(_v[1]);
	}

	inline bool isZero() {
		return _v[0] == 0.0f && _v[1] == 0.0f;
	}

	inline bool belongsToRect(T left, T top, T width, T height) const {
		return _v[0] >= left && _v[0] <= left + width && _v[1] >= top && _v[1] <= top + height;
	}

	static Vector2Generic<T> Min(const Vector2Generic<T> & v1, const Vector2Generic<T> & v2) {
		Vector2Generic<T> result;

		result._v[0] = bg::math::min(v1._v[0], v2._v[0]);
		result._v[1] = bg::math::min(v1._v[1], v2._v[1]);

		return result;
	}

	static Vector2Generic<T> Max(const Vector2Generic<T> & v1, const Vector2Generic<T> & v2) {
		Vector2Generic<T> result;

		result._v[0] = bg::math::max(v1._v[0], v2._v[0]);
		result._v[1] = bg::math::max(v1._v[1], v2._v[1]);

		return result;
	}

protected:
	T _v[2];
};

template <class T>
inline Vector2Generic<T> operator + (const Vector2Generic<T> & v1, const Vector2Generic<T> & v2) {
	return Vector2Generic<T>(v1.x() + v2.x(), v1.y() + v2.y());
}
template <class T>
inline Vector2Generic<T> operator - (const Vector2Generic<T> & v1, const Vector2Generic<T> & v2) {
	return Vector2Generic<T>(v1.x() - v2.x(), v1.y() - v2.y());
}
template <class T>
inline Vector2Generic<T> operator * (const Vector2Generic<T> & v1, const Vector2Generic<T> & v2) {
	return Vector2Generic<T>(v1.x() * v2.x(), v1.y() * v2.y());
}
template <class T>
inline Vector2Generic<T> operator / (const Vector2Generic<T> & v1, const Vector2Generic<T> & v2) {
	return Vector2Generic<T>(v1.x() / v2.x(), v1.y() / v2.y());
}
template <class T>
inline Vector2Generic<T> operator + (const Vector2Generic<T> & v1, T s) {
	return Vector2Generic<T>(v1.x() + s, v1.y() + s);
}
template <class T>
inline Vector2Generic<T> operator + (T s, const Vector2Generic<T> & v1) {
	return Vector2Generic<T>(v1.x() + s, v1.y() + s);
}
template <class T>
inline Vector2Generic<T> operator - (const Vector2Generic<T> & v1, T s) {
	return Vector2Generic<T>(v1.x() - s, v1.y() - s);
}
template <class T>
inline Vector2Generic<T> operator * (const Vector2Generic<T> & v1, T s) {
	return Vector2Generic<T>(v1.x() * s, v1.y() * s);
}
template <class T>
inline Vector2Generic<T> operator * (T s, const Vector2Generic<T> & v1) {
	return Vector2Generic<T>(v1.x() * s, v1.y() * s);
}
template <class T>
inline Vector2Generic<T> operator / (const Vector2Generic<T> & v1, T s) {
	return Vector2Generic<T>(v1.x() / s, v1.y() / s);
}
template <class T>
inline real_t dot(const Vector2Generic<T> & v1, const Vector2Generic<T> & v2) {
	return static_cast<real_t>(v1[0] * v2[0] + v1[1] * v2[1]);
}
template <class T>
inline Vector2Generic<T> unitVector(const Vector2Generic<T> & v) {
	return v / v.magnitude();
}

typedef Vector2Generic<short> Vector2s;
typedef Vector2Generic<int> Vector2i;
typedef Vector2Generic<long long> Vector2l;
typedef Vector2Generic<unsigned short> Vector2us;
typedef Vector2Generic<unsigned int> Vector2ui;
typedef Vector2Generic<unsigned long long> Vector2ul;
typedef Vector2Generic<real_t> Vector2f;
typedef Vector2Generic<double> Vector2d;

template <class T>
class Vector3Generic {
public:
	Vector3Generic() { _v[0] = static_cast<T>(0); _v[1] = static_cast<T>(0); _v[2] = static_cast<T>(0); }
	Vector3Generic(T s) { _v[0] = s; _v[1] = s; _v[2] = s; }
	Vector3Generic(T x, T y, T z) { _v[0] = x; _v[1] = y; _v[2] = z; }
	Vector3Generic(const T * v) { _v[0] = v[0]; _v[1] = v[1]; _v[2] = v[2]; }
	Vector3Generic(const Vector3Generic<T> & v) { _v[0] = v._v[0]; _v[1] = v._v[1]; _v[2] = v._v[2]; }
	Vector3Generic(const Vector2Generic<T> & v, T z) { _v[0] = v[0]; _v[1] = v[1]; _v[2] = z; }
	Vector3Generic(const Scalar & s) { _v[0] = static_cast<T>(s.value()); _v[1] = static_cast<T>(s.value()); _v[2] = static_cast<T>(s.value()); }
	Vector3Generic(const Scalar & x, const Scalar & y, const Scalar & z) { _v[0] = static_cast<T>(x.value()); _v[1] = static_cast<T>(y.value()); _v[2] = static_cast<T>(z.value()); }

	static Vector3Generic Lerp(const Vector3Generic<T> & from, const Vector3Generic<T> & to, real_t delta) {
		return Vector3Generic<T>(bg::math::lerp(from._v[0], to._v[0], delta),
			bg::math::lerp(from._v[1], to._v[1], delta),
			bg::math::lerp(from._v[2], to._v[2], delta));
	}


	inline T * raw() { return _v; }
	inline const T * raw() const { return _v; }
	inline int length() const { return 3; }

	inline real_t distance(const Vector3Generic<T> & v) const {
		Vector3Generic<T> v3(_v[0] - v._v[0],
			_v[1] - v._v[1],
			_v[2] - v._v[2]);
		return v3.magnitude();
	}

	inline real_t magnitude() const { return bg::math::sqrt(static_cast<real_t>(_v[0] * _v[0] + _v[1] * _v[1] + _v[2] * _v[2])); }
	inline real_t squaredMagnitude() const { return static_cast<real_t>(_v[0] * _v[0] + _v[1] * _v[1] + _v[2] * _v[2]); }
	inline Vector3Generic<T> & normalize() {
		T m = magnitude();
		_v[0] = _v[0] / m; _v[1] = _v[1] / m; _v[2] = _v[2] / m;
		return *this;
	}
	inline Vector3Generic<T> & add(const Vector3Generic<T> & v2) {
		_v[0] += v2._v[0];
		_v[1] += v2._v[1];
		_v[2] += v2._v[2];
		return *this;
	}
	inline Vector3Generic<T> & sub(const Vector3Generic<T> & v2) {
		_v[0] -= v2._v[0];
		_v[1] -= v2._v[1];
		_v[2] -= v2._v[2];
		return *this;
	}
	inline T dot(const Vector3Generic<T> & v2) const {
		return _v[0] * v2._v[0] + _v[1] * v2._v[1] + _v[2] * v2._v[2];
	}
	inline Vector3Generic<T> & scale(T scale) {
		_v[0] *= scale; _v[1] *= scale; _v[2] *= scale;
		return *this;
	}
	inline Vector3Generic<T> & scale(const Scalar & scale) {
		_v[0] *= static_cast<T>(scale.value()); _v[1] *= static_cast<T>(scale.value()); _v[2] *= static_cast<T>(scale.value());
		return *this;
	}

	inline T operator[](int i) const { return _v[i]; }
	inline T& operator[](int i) { return _v[i]; }
	inline bool operator==(const Vector3Generic<T> & v) const { return _v[0] == v._v[0] && _v[1] == v._v[1] && _v[2] == v._v[2]; }
	inline bool operator<(const Vector3Generic<T> & v) const { return _v[0] < v._v[0] && _v[1] < v._v[1] && _v[2] < v._v[2]; }
	inline bool operator>(const Vector3Generic<T> & v) const { return _v[0] > v._v[0] && _v[1] > v._v[1] && _v[2] > v._v[2]; }
	inline bool operator<=(const Vector3Generic<T> & v) const { return _v[0] <= v._v[0] && _v[1] <= v._v[1] && _v[2] <= v._v[2]; }
	inline bool operator>=(const Vector3Generic<T> & v) const { return _v[0] >= v._v[0] && _v[1] >= v._v[1] && _v[2] >= v._v[2]; }
	inline bool operator!=(const Vector3Generic<T> & v) const { return _v[0] != v._v[0] || _v[1] != v._v[1] || _v[2] != v._v[2]; }
	inline void operator=(const Vector3Generic<T> & v) { _v[0] = v._v[0]; _v[1] = v._v[1]; _v[2] = v._v[2]; }
	inline void operator=(const T * v) { _v[0] = v[0]; _v[1] = v[1]; _v[2] = v[2]; }

	inline Vector3Generic<T> & operator +=(const Vector3Generic<T> & v) { _v[0] += v->_v[0]; _v[1] += v->_v[1]; _v[2] += v->_v[2]; return *this; }
	inline Vector3Generic<T> & operator -=(const Vector3Generic<T> & v) { _v[0] -= v->_v[0]; _v[1] -= v->_v[1]; _v[2] -= v->_v[2]; return *this; }
	inline Vector3Generic<T> & operator *=(const Vector3Generic<T> & v) { _v[0] *= v->_v[0]; _v[1] *= v->_v[1]; _v[2] *= v->_v[2]; return *this; }
	inline Vector3Generic<T> & operator /=(const Vector3Generic<T> & v) { _v[0] /= v->_v[0]; _v[1] /= v->_v[1]; _v[2] /= v->_v[2]; return *this; }
	inline Vector3Generic<T> & operator *=(const T v) { _v[0] *= v; _v[1] *= v; _v[2] *= v; return *this; }
	inline Vector3Generic<T> & operator /=(const T v) { T k = static_cast<T>(1) / static_cast<T>(v); _v[0] *= k; _v[1] *= k; _v[2] *= k; return *this; }


	inline std::string toString() const {
		std::stringstream ss;
		ss << '[' << _v[0] << "," << _v[1] << "," << _v[2] << ']';
		return ss.str();
	}

	inline void set(T v) { _v[0] = v; _v[1] = v; _v[2] = v; }
	inline void set(T x, T y, T z) { _v[0] = x; _v[1] = y; _v[2] = z; }
	inline void set(const Scalar & v) { _v[0] = static_cast<T>(v.value()); _v[1] = static_cast<T>(v.value()); _v[2] = static_cast<T>(v.value()); }
	inline void set(const Scalar & x, const Scalar & y, const Scalar & z) { _v[0] = static_cast<T>(x.value()); _v[1] = static_cast<T>(y.value()); _v[2] = static_cast<T>(z.value()); }

	inline T x() const { return _v[0]; }
	inline T y() const { return _v[1]; }
	inline T z() const { return _v[2]; }

	inline void x(T v) { _v[0] = v; }
	inline void y(T v) { _v[1] = v; }
	inline void z(T v) { _v[2] = v; }
	inline void x(const Scalar & v) { _v[0] = static_cast<T>(v.value()); }
	inline void y(const Scalar & v) { _v[1] = static_cast<T>(v.value()); }
	inline void z(const Scalar & v) { _v[2] = static_cast<T>(v.value()); }

	inline T r() const { return _v[0]; }
	inline T g() const { return _v[1]; }
	inline T b() const { return _v[2]; }

	inline void r(T v) { _v[0] = v; }
	inline void g(T v) { _v[1] = v; }
	inline void b(T v) { _v[2] = v; }

	inline T width() const { return _v[0]; }
	inline T height() const { return _v[1]; }
	inline T depth() const { return _v[2]; }

	inline void width(T v) { _v[0] = v; }
	inline void height(T v) { _v[1] = v; }
	inline void depth(T v) { _v[2] = v; }
	inline void width(const Scalar & v) { _v[0] = static_cast<T>(v.value()); }
	inline void height(const Scalar & v) { _v[1] = static_cast<T>(v.value()); }
	inline void depth(const Scalar & v) { _v[2] = static_cast<T>(v.value()); }

	inline Vector2Generic<T> xy() const { return Vector2Generic<T>(x(), y()); }
	inline Vector2Generic<T> yz() const { return Vector2Generic<T>(y(), z()); }
	inline Vector2Generic<T> xz() const { return Vector2Generic<T>(x(), z()); }

	inline T aspectRatio() const { return static_cast<T>(_v[0]) / static_cast<T>(_v[1]); }

	inline Vector3Generic<T> & clamp(T min, T max) {
		_v[0] = number::clamp<T>(_v[0], min, max);
		_v[1] = number::clamp<T>(_v[1], min, max);
		_v[2] = number::clamp<T>(_v[2], min, max);
		return *this;
	}

	inline Vector3Generic<T> & clamp(const Scalar & min, const Scalar & max) {
		_v[0] = number::clamp<T>(_v[0], static_cast<T>(min.value()), static_cast<T>(max.value()));
		_v[1] = number::clamp<T>(_v[1], static_cast<T>(min.value()), static_cast<T>(max.value()));
		_v[2] = number::clamp<T>(_v[2], static_cast<T>(min.value()), static_cast<T>(max.value()));
		return *this;
	}

	inline bool isNan() {
#ifdef _WIN32
		return _isnan(_v[0]) || _isnan(_v[1]) || _isnan(_v[2]);
#else
		return isnan(_v[0]) || isnan(_v[1]) || isnan(_v[2]);
#endif
	}

	inline bool isZero() {
		return _v[0] == 0.0f && _v[1] == 0.0f && _v[2] == 0.0f;
	}

	static Vector3Generic<T> Min(const Vector3Generic<T> & v1, const Vector3Generic<T> & v2) {
		Vector3Generic<T> result;

		result._v[0] = bg::math::min(v1._v[0], v2._v[0]);
		result._v[1] = bg::math::min(v1._v[1], v2._v[1]);
		result._v[2] = bg::math::min(v1._v[2], v2._v[2]);

		return result;
	}

	static Vector3Generic<T> Max(const Vector3Generic<T> & v1, const Vector3Generic<T> & v2) {
		Vector3Generic<T> result;

		result._v[0] = bg::math::max(v1._v[0], v2._v[0]);
		result._v[1] = bg::math::max(v1._v[1], v2._v[1]);
		result._v[2] = bg::math::max(v1._v[2], v2._v[2]);

		return result;
	}

protected:
	T _v[3];
};

template <class T>
inline Vector3Generic<T> operator + (const Vector3Generic<T> & v1, const Vector3Generic<T> & v2) {
	return Vector3Generic<T>(v1.x() + v2.x(), v1.y() + v2.y(), v1.z() + v2.z());
}
template <class T>
inline Vector3Generic<T> operator - (const Vector3Generic<T> & v1, const Vector3Generic<T> & v2) {
	return Vector3Generic<T>(v1.x() - v2.x(), v1.y() - v2.y(), v1.z() - v2.z());
}
template <class T>
inline Vector3Generic<T> operator * (const Vector3Generic<T> & v1, const Vector3Generic<T> & v2) {
	return Vector3Generic<T>(v1.x() * v2.x(), v1.y() * v2.y(), v1.z() * v2.z());
}
template <class T>
inline Vector3Generic<T> operator / (const Vector3Generic<T> & v1, const Vector3Generic<T> & v2) {
	return Vector3Generic<T>(v1.x() / v2.x(), v1.y() / v2.y(), v1.z() / v2.z());
}
template <class T>
inline Vector3Generic<T> operator + (const Vector3Generic<T> & v1, T s) {
	return Vector3Generic<T>(v1.x() + s, v1.y() + s, v1.z() + s);
}
template <class T>
inline Vector3Generic<T> operator + (T s, const Vector3Generic<T> & v1) {
	return Vector3Generic<T>(v1.x() + s, v1.y() + s, v1.z() + s);
}
template <class T>
inline Vector3Generic<T> operator - (const Vector3Generic<T> & v1, T s) {
	return Vector3Generic<T>(v1.x() - s, v1.y() - s, v1.z() - s);
}
template <class T>
inline Vector3Generic<T> operator - (T s, const Vector3Generic<T> & v1) {
	return Vector3Generic<T>(s - v1.x(), s - v1.y(), s - v1.z());
}
template <class T>
inline Vector3Generic<T> operator * (const Vector3Generic<T> & v1, T s) {
	return Vector3Generic<T>(v1.x() * s, v1.y() * s, v1.z() * s);
}
template <class T>
inline Vector3Generic<T> operator * (T s, const Vector3Generic<T> & v1) {
	return Vector3Generic<T>(v1.x() * s, v1.y() * s, v1.z() * s);
}
template <class T>
inline Vector3Generic<T> operator / (const Vector3Generic<T> & v1, T s) {
	return Vector3Generic<T>(v1.x() / s, v1.y() / s, v1.z() / s);
}
template <class T>
inline Vector3Generic<T> operator / (T s, const Vector3Generic<T> & v1) {
	return Vector3Generic<T>(s / v1.x(), s / v1.y(), s / v1.z());
}
template <class T>
inline real_t dot(const Vector3Generic<T> & v1, const Vector3Generic<T> & v2) {
	return static_cast<real_t>(v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2]);
}
template <class T>
inline Vector3Generic<T> cross(const Vector3Generic<T> & u, const Vector3Generic<T> & v) {
	return Vector3Generic<T>(
		u[1] * v[2] - u[2] * v[1],
		u[2] * v[0] - u[0] * v[2],
		u[0] * v[1] - u[1] * v[0]
		);
}
template <class T>
inline Vector3Generic<T> unitVector(const Vector3Generic<T> & v) {
	Vector3Generic<T> res = v;
	res.normalize();
	return res;
}

typedef Vector3Generic<char> Vector3c;
typedef Vector3Generic<unsigned char> Vector3uc;
typedef Vector3Generic<short> Vector3s;
typedef Vector3Generic<int> Vector3i;
typedef Vector3Generic<long> Vector3l;
typedef Vector3Generic<unsigned short> Vector3us;
typedef Vector3Generic<unsigned int> Vector3ui;
typedef Vector3Generic<unsigned long> Vector3ul;
typedef Vector3Generic<real_t> Vector3f;
typedef Vector3Generic<double> Vector3d;

template <class T>
class Vector4Generic {
public:
	static Vector4Generic Yellow() { return Vector4Generic(1.0f, 1.0f, 0.0f, 1.0f); }
	static Vector4Generic Orange() { return Vector4Generic(1.0f, 0.5f, 0.0f, 1.0f); }
	static Vector4Generic Red() { return Vector4Generic(1.0f, 0.0f, 0.0f, 1.0f); }
	static Vector4Generic Pink() { return Vector4Generic(1.0f, 0.5f, 0.5f, 1.0f); }
	static Vector4Generic Violet() { return Vector4Generic(0.5f, 0.0f, 1.0f, 1.0f); }
	static Vector4Generic Blue() { return Vector4Generic(0.0f, 0.0f, 1.0f, 1.0f); }
	static Vector4Generic Green() { return Vector4Generic(0.0f, 1.0f, 0.0f, 1.0f); }
	static Vector4Generic White() { return Vector4Generic(1.0f, 1.0f, 1.0f, 1.0f); }
	static Vector4Generic LightGray() { return Vector4Generic(0.8f, 0.8f, 0.8f, 1.0f); }
	static Vector4Generic Gray() { return Vector4Generic(0.5f, 0.5f, 0.5f, 1.0f); }
	static Vector4Generic DarkGray() { return Vector4Generic(0.2f, 0.2f, 0.2f, 1.0f); }
	static Vector4Generic Black() { return Vector4Generic(0.0f, 0.0f, 0.0f, 1.0f); }
	static Vector4Generic Brown() { return Vector4Generic(0.4f, 0.2f, 0.0f, 1.0f); }
	static Vector4Generic Transparent() { return Vector4Generic(0.0f); }

	static Vector4Generic<T> Lerp(const Vector4Generic<T> & from, const Vector4Generic<T> & to, real_t delta) {
		return Vector4Generic<T>(bg::math::lerp(from._v[0], to._v[0], delta),
			bg::math::lerp(from._v[1], to._v[1], delta),
			bg::math::lerp(from._v[2], to._v[2], delta),
			bg::math::lerp(from._v[3], to._v[3], delta));
	}


	Vector4Generic() { _v[0] = static_cast<T>(0); _v[1] = static_cast<T>(0); _v[2] = static_cast<T>(0); _v[3] = static_cast<T>(0); }
	Vector4Generic(T s) { _v[0] = s; _v[1] = s; _v[2] = s; _v[3] = s; }
	Vector4Generic(T x, T y, T z, T w) { _v[0] = x; _v[1] = y; _v[2] = z; _v[3] = w; }
	Vector4Generic(const T * v) { _v[0] = v[0]; _v[1] = v[1]; _v[2] = v[2]; _v[3] = v[3]; }
	Vector4Generic(const Vector4Generic<T> & v) { _v[0] = v._v[0]; _v[1] = v._v[1]; _v[2] = v._v[2]; _v[3] = v._v[3]; }
	Vector4Generic(const Vector3Generic<T> & v, T w) { _v[0] = v[0]; _v[1] = v[1]; _v[2] = v[2]; _v[3] = w; }
	Vector4Generic(const Vector2Generic<T> & v, T z, T w) { _v[0] = v[0]; _v[1] = v[1]; _v[2] = z; _v[3] = w; }
	Vector4Generic(const Scalar & s)
	{
		_v[0] = static_cast<T>(s.value()); _v[1] = static_cast<T>(s.value()); _v[2] = static_cast<T>(s.value()); _v[3] = static_cast<T>(s.value());
	}
	Vector4Generic(const Scalar & x, const Scalar & y, const Scalar & z, const Scalar & w)
	{
		_v[0] = static_cast<T>(x.value()); _v[1] = static_cast<T>(y.value()); _v[2] = static_cast<T>(z.value()); _v[3] = static_cast<T>(w.value());
	}

	inline T * raw() { return _v; }
	inline const T * raw() const { return _v; }
	inline int length() const { return 4; }

	inline real_t distance(const Vector4Generic<T> & v) const {
		Vector4Generic<T> v3(_v[0] - v._v[0],
			_v[1] - v._v[1],
			_v[2] - v._v[2],
			_v[3] - v._v[3]);
		return v3.magnitude();
	}

	inline T magnitude() const { return bg::math::sqrt(static_cast<T>(_v[0] * _v[0] + _v[1] * _v[1] + _v[2] * _v[2] + _v[3] * _v[3])); }
	inline T squaredMagnitude() const { return static_cast<T>(_v[0] * _v[0] + _v[1] * _v[1] + _v[2] * _v[2] + _v[3] * _v[3]); }
	inline Vector4Generic & normalize() {
		T m = magnitude();
		_v[0] = _v[0] / m; _v[1] = _v[1] / m; _v[2] = _v[2] / m; _v[3] = _v[3] / m;
		return *this;
	}

	inline T dot(const Vector4Generic<T> & v2) const {
		return _v[0] * v2._v[0] + _v[1] * v2._v[1] + _v[2] * v2._v[2] + _v[3] * v2._v[3];
	}

	inline T operator[](int i) const { return _v[i]; }
	inline T & operator[](int i) { return _v[i]; }
	inline bool operator==(const Vector4Generic & v) const { return _v[0] == v._v[0] && _v[1] == v._v[1] && _v[2] == v._v[2] && _v[3] == v._v[3]; }
	inline bool operator<(const Vector4Generic<T> & v) const { return _v[0] < v._v[0] && _v[1] < v._v[1] && _v[2] < v._v[2] && _v[3] < v._v[3]; }
	inline bool operator>(const Vector4Generic<T> & v) const { return _v[0] > v._v[0] && _v[1] > v._v[1] && _v[2] > v._v[2] && _v[3] > v._v[3]; }
	inline bool operator<=(const Vector4Generic<T> & v) const { return _v[0] <= v._v[0] && _v[1] <= v._v[1] && _v[2] <= v._v[2] && _v[3] <= v._v[3]; }
	inline bool operator>=(const Vector4Generic<T> & v) const { return _v[0] >= v._v[0] && _v[1] >= v._v[1] && _v[2] >= v._v[2] && _v[3] >= v._v[3]; }
	inline bool operator!=(const Vector4Generic & v) const { return _v[0] != v._v[0] || _v[1] != v._v[1] || _v[2] != v._v[2] || _v[3] != v._v[3]; }
	inline void operator=(const Vector4Generic & v) { _v[0] = v._v[0]; _v[1] = v._v[1]; _v[2] = v._v[2]; _v[3] = v._v[3]; }
	inline void operator=(const T * v) { _v[0] = v[0]; _v[1] = v[1]; _v[2] = v[2]; _v[3] = v[3]; }

	inline Vector4Generic<T> & operator +=(const Vector4Generic<T> & v) { _v[0] += v->_v[0]; _v[1] += v->_v[1]; _v[2] += v->_v[2]; _v[3] += v->_v[3]; return *this; }
	inline Vector4Generic<T> & operator -=(const Vector4Generic<T> & v) { _v[0] -= v->_v[0]; _v[1] -= v->_v[1]; _v[2] -= v->_v[2]; _v[3] -= v->_v[3]; return *this; }
	inline Vector4Generic<T> & operator *=(const Vector4Generic<T> & v) { _v[0] *= v->_v[0]; _v[1] *= v->_v[1]; _v[2] *= v->_v[2]; _v[3] *= v->_v[3]; return *this; }
	inline Vector4Generic<T> & operator /=(const Vector4Generic<T> & v) { _v[0] /= v->_v[0]; _v[1] /= v->_v[1]; _v[2] /= v->_v[2]; _v[3] /= v->_v[3]; return *this; }
	inline Vector4Generic<T> & operator *=(const T v) { _v[0] *= v; _v[1] *= v; _v[2] *= v; _v[3] *= v; return *this; }
	inline Vector4Generic<T> & operator /=(const T v) { T k = static_cast<T>(1) / static_cast<T>(v); _v[0] *= k; _v[1] *= k; _v[2] *= k; _v[3] *= k; return *this; }


	inline Vector4Generic operator+(const Vector4Generic &v) const { return Vector4Generic(_v[0] + v._v[0], _v[1] + v._v[1], _v[2] + v._v[2], _v[3] + v._v[3]); }
	inline Vector4Generic operator-(const Vector4Generic &v) const { return Vector4Generic(_v[0] - v._v[0], _v[1] - v._v[1], _v[2] - v._v[2], _v[3] - v._v[3]); }
	inline T operator*(const Vector4Generic &v) const { return _v[0] * v._v[0] + _v[1] * v._v[1] + _v[2] * v._v[2] + +_v[3] * v._v[3]; }
	inline Vector4Generic operator/(T scalar) const { return Vector4Generic(_v[0] / scalar, _v[1] / scalar, _v[2] / scalar, _v[3] / scalar); }

	inline std::string toString() const {
		std::stringstream ss;
		ss << '[' << _v[0] << "," << _v[1] << "," << _v[2] << "," << _v[3] << ']';
		return ss.str();
	}

	inline void set(T v) { _v[0] = v; _v[1] = v; _v[2] = v; _v[3] = v; }
	inline void set(T x, T y, T z, T w) { _v[0] = x; _v[1] = y; _v[2] = z; _v[3] = w; }
	inline void set(const Scalar & s)
	{
		_v[0] = static_cast<T>(s.value()); _v[1] = static_cast<T>(s.value()); _v[2] = static_cast<T>(s.value()); _v[3] = static_cast<T>(s.value());
	}
	inline void set(const Scalar & x, const Scalar & y, const Scalar & z, const Scalar & w)
	{
		_v[0] = static_cast<T>(x.value()); _v[1] = static_cast<T>(y.value()); _v[2] = static_cast<T>(z.value()); _v[3] = static_cast<T>(w.value());
	}

	inline T x() const { return _v[0]; }
	inline T y() const { return _v[1]; }
	inline T z() const { return _v[2]; }
	inline T w() const { return _v[3]; }

	inline void x(T v) { _v[0] = v; }
	inline void y(T v) { _v[1] = v; }
	inline void z(T v) { _v[2] = v; }
	inline void w(T v) { _v[3] = v; }

	inline void x(const Scalar & v) { _v[0] = static_cast<T>(v.value()); }
	inline void y(const Scalar & v) { _v[1] = static_cast<T>(v.value()); }
	inline void z(const Scalar & v) { _v[2] = static_cast<T>(v.value()); }
	inline void w(const Scalar & v) { _v[3] = static_cast<T>(v.value()); }

	inline T r() const { return _v[0]; }
	inline T g() const { return _v[1]; }
	inline T b() const { return _v[2]; }
	inline T a() const { return _v[3]; }

	inline void r(T v) { _v[0] = v; }
	inline void g(T v) { _v[1] = v; }
	inline void b(T v) { _v[2] = v; }
	inline void a(T v) { _v[3] = v; }


	inline Vector2Generic<T> xy() const { return Vector2Generic<T>(x(), y()); }
	inline Vector2Generic<T> xz() const { return Vector2Generic<T>(x(), z()); }
	inline Vector2Generic<T> xw() const { return Vector2Generic<T>(x(), w()); }
	inline Vector3Generic<T> xyz() const { return Vector3Generic<T>(x(), y(), z()); }

	inline T width() const { return _v[2]; }
	inline T height() const { return _v[3]; }

	inline void width(T v) { _v[2] = v; }
	inline void height(T v) { _v[3] = v; }
	inline void width(const Scalar & v) { _v[2] = static_cast<T>(v.value()); }
	inline void height(const Scalar & v) { _v[3] = static_cast<T>(v.value()); }

	inline real_t aspectRatio() const { return _v[3] != static_cast<T>(0) ? static_cast<real_t>(_v[2]) / static_cast<real_t>(_v[3]) : 1.0f; }

	inline Vector4Generic<T> & clamp(T min, T max) {
		_v[0] = number::clamp<T>(_v[0], min, max);
		_v[1] = number::clamp<T>(_v[1], min, max);
		_v[2] = number::clamp<T>(_v[2], min, max);
		_v[3] = number::clamp<T>(_v[3], min, max);
		return *this;
	}

	inline bool isNan() {
		return isnan(_v[0]) || isnan(_v[1]) || isnan(_v[2]) || isnan(_v[3]);
	}

	inline bool isZero() {
		return _v[0] == 0.0f && _v[1] == 0.0f && _v[2] == 0.0f && _v[3] == 0.0f;
	}

	inline Vector4Generic<T> & scale(T scale) {
		_v[0] *= scale; _v[1] *= scale; _v[2] *= scale; _v[3] *= scale;
		return *this;
	}
	inline Vector4Generic<T> & scale(const Scalar & scale) {
		_v[0] *= static_cast<T>(scale.value());
		_v[1] *= static_cast<T>(scale.value());
		_v[2] *= static_cast<T>(scale.value());
		_v[3] *= static_cast<T>(scale.value());
		return *this;
	}

	// Viewport contains 2D point
	inline bool containsPoint(const Vector2Generic<T> & p) const {
		return _v[0] <= p.x() && _v[0] + _v[2] >= p.x() &&
			_v[1] <= p.y() && _v[1] + _v[3] >= p.y();
	}

	static Vector4Generic<T> Min(const Vector4Generic<T> & v1, const Vector4Generic<T> & v2) {
		Vector4Generic<T> result;

		result._v[0] = bg::math::min(v1._v[0], v2._v[0]);
		result._v[1] = bg::math::min(v1._v[1], v2._v[1]);
		result._v[2] = bg::math::min(v1._v[2], v2._v[2]);
		result._v[3] = bg::math::min(v1._v[3], v2._v[3]);

		return result;
	}

	static Vector4Generic<T> Max(const Vector4Generic<T> & v1, const Vector4Generic<T> & v2) {
		Vector4Generic<T> result;

		result._v[0] = bg::math::max(v1._v[0], v2._v[0]);
		result._v[1] = bg::math::max(v1._v[1], v2._v[1]);
		result._v[2] = bg::math::max(v1._v[2], v2._v[2]);
		result._v[3] = bg::math::max(v1._v[3], v2._v[3]);

		return result;
	}

protected:
	T _v[4];
};

template <class T>
inline Vector4Generic<T> operator + (const Vector4Generic<T> & v1, const Vector4Generic<T> & v2) {
	return Vector4Generic<T>(v1.x() + v2.x(), v1.y() + v2.y(), v1.z() + v2.z(), v1.w() + v2.w());
}
template <class T>
inline Vector4Generic<T> operator - (const Vector4Generic<T> & v1, const Vector4Generic<T> & v2) {
	return Vector4Generic<T>(v1.x() - v2.x(), v1.y() - v2.y(), v1.z() - v2.z(), v1.w() - v2.w());
}
template <class T>
inline Vector4Generic<T> operator * (const Vector4Generic<T> & v1, const Vector4Generic<T> & v2) {
	return Vector4Generic<T>(v1.x() * v2.x(), v1.y() * v2.y(), v1.z() * v2.z(), v1.w() * v2.w());
}
template <class T>
inline Vector4Generic<T> operator / (const Vector4Generic<T> & v1, const Vector4Generic<T> & v2) {
	return Vector4Generic<T>(v1.x() / v2.x(), v1.y() / v2.y(), v1.z() / v2.z(), v1.w() / v2.w());
}

template <class T>
inline Vector4Generic<T> operator + (const Vector4Generic<T> & v1, T s) {
	return Vector4Generic<T>(v1.x() + s, v1.y() + s, v1.z() + s, v1.w() + s);
}
template <class T>
inline Vector4Generic<T> operator + (T s, const Vector4Generic<T> & v1) {
	return Vector4Generic<T>(v1.x() + s, v1.y() + s, v1.z() + s, v1.w() + s);
}
template <class T>
inline Vector4Generic<T> operator - (const Vector4Generic<T> & v1, T s) {
	return Vector4Generic<T>(v1.x() - s, v1.y() - s, v1.z() - s, v1.w() - s);
}


template <class T>
inline Vector4Generic<T> operator * (const Vector4Generic<T> & v1, T s) {
	return Vector4Generic<T>(v1.x() * s, v1.y() * s, v1.z() * s, v1.w() * s);
}
template <class T>
inline Vector4Generic<T> operator * (T s, const Vector4Generic<T> & v1) {
	return Vector4Generic<T>(v1.x() * s, v1.y() * s, v1.z() * s, v1.w() * s);
}
template <class T>
inline Vector4Generic<T> operator / (const Vector4Generic<T> & v1, T s) {
	return Vector4Generic<T>(v1.x() / s, v1.y() / s, v1.z() / s, v1.w() / s);
}
template <class T>
inline real_t dot(const Vector4Generic<T> & v1, const Vector4Generic<T> & v2) {
	return static_cast<real_t>(v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2] + v1[3] * v2[3]);
}
template <class T>
inline Vector4Generic<T> unitVector(const Vector4Generic<T> & v) {
	return v / v.magnitude();
}

typedef Vector4Generic<short> Vector4s;
typedef Vector4Generic<int> Vector4i;
typedef Vector4Generic<long> Vector4l;
typedef Vector4Generic<unsigned short> Vector4us;
typedef Vector4Generic<unsigned int> Vector4ui;
typedef Vector4Generic<unsigned long> Vector4ul;
typedef Vector4Generic<real_t> Vector4f;
typedef Vector4Generic<double> Vector4d;

typedef Vector4f Color;
typedef Vector3f Size3D;
typedef Vector2f Size2D;
typedef Vector3i Size3Di;
typedef Vector2i Size2Di;
typedef Vector4i Viewport;
typedef Vector4i Rect;
typedef Vector2f Position2D;
typedef Vector2i Position2Di;

#ifdef BG2EMATH_PRECISION_DOUBLE

typedef Vector2d Vector2;
typedef Vector3d Vector3;
typedef Vector4d Vector4;

#else

typedef Vector2f Vector2;
typedef Vector3f Vector3;
typedef Vector4f Vector4;

#endif


    

class Matrix3 {
public:
    Matrix3() {
        _m[0] = static_cast<real_t>(0.0); _m[1] = static_cast<real_t>(0.0); _m[2] = static_cast<real_t>(0.0);
        _m[3] = static_cast<real_t>(0.0); _m[4] = static_cast<real_t>(0.0); _m[5] = static_cast<real_t>(0.0);
        _m[6] = static_cast<real_t>(0.0); _m[7] = static_cast<real_t>(0.0); _m[8] = static_cast<real_t>(0.0);
    }
    Matrix3(float s) {
        _m[0] = s; _m[1] = s; _m[2] = s;
        _m[3] = s; _m[4] = s; _m[5] = s;
        _m[6] = s; _m[7] = s; _m[8] = s;
    }
    Matrix3(real_t m00, real_t m01, real_t m02,
            real_t m10, real_t m11, real_t m12,
            real_t m20, real_t m21, real_t m22) {
        _m[0] = m00; _m[1] = m01; _m[2] = m02;
        _m[3] = m10; _m[4] = m11; _m[5] = m12;
        _m[6] = m20; _m[7] = m21; _m[8] = m22;
    }
    Matrix3(const Vector3 & r1, const Vector3 & r2, const Vector3 & r3) {
        _m[0] = r1[0]; _m[1] = r1[1]; _m[2] = r1[2];
        _m[3] = r2[0]; _m[4] = r2[1]; _m[5] = r2[2];
        _m[6] = r3[0]; _m[7] = r3[1]; _m[8] = r3[2];
    }
    Matrix3(const real_t * m) {
        _m[0] = m[0]; _m[1] = m[1]; _m[2] = m[2];
        _m[3] = m[3]; _m[4] = m[4]; _m[5] = m[5];
        _m[6] = m[6]; _m[7] = m[7]; _m[8] = m[8];
    }
    Matrix3(const Matrix3 & m) {
        _m[0] = m._m[0]; _m[1] = m._m[1]; _m[2] = m._m[2];
        _m[3] = m._m[3]; _m[4] = m._m[4]; _m[5] = m._m[5];
        _m[6] = m._m[6]; _m[7] = m._m[7]; _m[8] = m._m[8];
    }
    
    inline bool isZero() {
        return  _m[0]==static_cast<real_t>(0.0) && _m[1]==static_cast<real_t>(0.0) && _m[2]==static_cast<real_t>(0.0) &&
                _m[3]==static_cast<real_t>(0.0) && _m[4]==static_cast<real_t>(0.0) && _m[5]==static_cast<real_t>(0.0) &&
                _m[6]==static_cast<real_t>(0.0) && _m[7]==static_cast<real_t>(0.0) && _m[8]==static_cast<real_t>(0.0);
    }
    
    inline bool isIdentity() {
        return  _m[0]==static_cast<real_t>(1.0) && _m[1]==static_cast<real_t>(0.0) && _m[2]==static_cast<real_t>(0.0) &&
                _m[3]==static_cast<real_t>(0.0) && _m[4]==static_cast<real_t>(1.0) && _m[5]==static_cast<real_t>(0.0) &&
                _m[6]==static_cast<real_t>(0.0) && _m[7]==static_cast<real_t>(0.0) && _m[8]==static_cast<real_t>(1.0);
    }
    
    static Matrix3 Identity() {
        Matrix3 i;
        i._m[0] = static_cast<real_t>(1.0); i._m[1] = static_cast<real_t>(0.0); i._m[2] = static_cast<real_t>(0.0);
        i._m[3] = static_cast<real_t>(0.0); i._m[4] = static_cast<real_t>(1.0); i._m[5] = static_cast<real_t>(0.0);
        i._m[6] = static_cast<real_t>(0.0); i._m[7] = static_cast<real_t>(0.0); i._m[8] = static_cast<real_t>(1.0);
        return i;
    }
    
    inline Matrix3 & identity() {
        _m[0] = static_cast<real_t>(1.0); _m[1] = static_cast<real_t>(0.0); _m[2] = static_cast<real_t>(0.0);
        _m[3] = static_cast<real_t>(0.0); _m[4] = static_cast<real_t>(1.0); _m[5] = static_cast<real_t>(0.0);
        _m[6] = static_cast<real_t>(0.0); _m[7] = static_cast<real_t>(0.0); _m[8] = static_cast<real_t>(1.0);
        
        return *this;
    }
    
    inline const real_t * row(int i) const { return _m + (i*3); }
    inline void setRow(int i, const Vector3 & r) { _m[i*3]=r[0]; _m[i*3+1]=r[1]; _m[i*3+2]=r[2]; }
    inline void set(real_t s) {
        _m[ 0]=s; _m[ 1]=s; _m[ 2]=s;
        _m[ 3]=s; _m[ 4]=s; _m[ 5]=s;
        _m[ 6]=s; _m[ 7]=s; _m[ 8]=s;
    }
    
    inline const real_t * raw() const { return _m; }
    inline int length() const { return 9; }
    
    inline Matrix3 & transpose() {
        Vector3 r0(_m[0], _m[3], _m[6]);
        Vector3 r1(_m[1], _m[4], _m[7]);
        Vector3 r2(_m[2], _m[5], _m[8]);
        
        setRow(0, r0);
        setRow(1, r1);
        setRow(2, r2);
        
        return *this;
    }
    
    inline real_t operator[](int i) const { return _m[i]; }
    inline void operator=(const real_t * m) {
        _m[0] = m[0]; _m[1] = m[1]; _m[2] = m[2];
        _m[3] = m[3]; _m[4] = m[4]; _m[5] = m[5];
        _m[6] = m[6]; _m[7] = m[7]; _m[8] = m[8];
    }
    inline void operator=(const Matrix3 & m) {
        _m[0] = m._m[0]; _m[1] = m._m[1]; _m[2] = m._m[2];
        _m[3] = m._m[3]; _m[4] = m._m[4]; _m[5] = m._m[5];
        _m[6] = m._m[6]; _m[7] = m._m[7]; _m[8] = m._m[8];
    }
    inline bool operator==(const Matrix3 & m) const {
        return  _m[0] == m._m[0] && _m[1] == m._m[1]  && _m[2] == m._m[2] &&
                _m[3] == m._m[3] && _m[4] == m._m[4]  && _m[5] == m._m[5] &&
                _m[6] == m._m[6] && _m[7] == m._m[7]  && _m[8] == m._m[8];
    }
    inline bool operator!=(const Matrix3 & m) const {
        return  _m[0] != m._m[0] || _m[1] != m._m[1] || _m[2] != m._m[2] ||
                _m[3] != m._m[3] || _m[4] != m._m[4] || _m[5] != m._m[5] ||
                _m[6] != m._m[6] || _m[7] != m._m[7] || _m[8] != m._m[8];
    }
    
    inline std::string toString() const {
        std::stringstream ss;
        
        ss  << '[' << _m[0] << "," << _m[1] << "," << _m[2] << "," << std::endl << ' '
            << _m[3] << "," << _m[4] << "," << _m[5] << "," << std::endl << ' '
            << _m[6] << "," << _m[7] << "," << _m[8] << ']';
        
        return ss.str();
    }
    
    inline Matrix3 & mult(const Matrix3 & rMatrix) {
        //        _m[0] _m[1] _m[2]        rm[0] rm[1] rm[2]
        //        _m[3] _m[4] _m[5]        rm[3] rm[4] rm[5]
        //        _m[6] _m[7] _m[8]        rm[6] rm[7] rm[8]
        const real_t * rm = _m;
        const real_t * lm = rMatrix.raw();
        Matrix3 res;
        
        res._m[0] = lm[0] * rm[0] + lm[1] * rm[1] + lm[2] * rm[2];
        res._m[1] = lm[0] * rm[1] + lm[1] * rm[4] + lm[2] * rm[7];
        res._m[2] = lm[0] * rm[2] + lm[1] * rm[5] + lm[2] * rm[8];
        
        res._m[3] = lm[3] * rm[0] + lm[4] * rm[3] + lm[5] * rm[6];
        res._m[4] = lm[3] * rm[1] + lm[4] * rm[4] + lm[5] * rm[7];
        res._m[5] = lm[3] * rm[2] + lm[4] * rm[5] + lm[5] * rm[8];
        
        res._m[6] = lm[6] * rm[0] + lm[7] * rm[3] + lm[8] * rm[6];
        res._m[7] = lm[6] * rm[1] + lm[7] * rm[4] + lm[8] * rm[7];
        res._m[8] = lm[6] * rm[2] + lm[7] * rm[5] + lm[8] * rm[8];
        
        (*this) = res;
        return *this;
    }
    
    inline Matrix3 & mult(real_t s) {
        _m[0] *= s; _m[1] *= s; _m[2] *= s;
        _m[3] *= s; _m[4] *= s; _m[5] *= s;
        _m[6] *= s; _m[7] *= s; _m[8] *= s;
        return *this;
    }
    
    void set00(real_t v) { _m[0] = v; }
    void set01(real_t v) { _m[1] = v; }
    void set02(real_t v) { _m[2] = v; }
    
    void set10(real_t v) { _m[3] = v; }
    void set11(real_t v) { _m[4] = v; }
    void set12(real_t v) { _m[5] = v; }
    
    void set20(real_t v) { _m[6] = v; }
    void set21(real_t v) { _m[7] = v; }
    void set22(real_t v) { _m[8] = v; }
    
    real_t get00() const { return _m[0]; }
    real_t get01() const { return _m[1]; }
    real_t get02() const { return _m[2]; }
    
    real_t get10() const { return _m[3]; }
    real_t get11() const { return _m[4]; }
    real_t get12() const { return _m[5]; }
    
    real_t get20() const { return _m[6]; }
    real_t get21() const { return _m[7]; }
    real_t get22() const { return _m[8]; }
    
    inline Vector3 eulerAngles() const {
        real_t rx = trigonometry::atan2(get12(), get22());
        real_t c2 = sqrt(get00() * get00() + get01() * get01());
        real_t ry = trigonometry::atan2(-get02(),c2);
        real_t s1 = trigonometry::sin(rx);
        real_t c1 = trigonometry::cos(rx);
        real_t rz = trigonometry::atan2(s1 * get20() - c1 * get10(), c1 * get11() - s1 * get21());
        return Vector3(rx,ry,rz);
    }
    
    inline bool isNan() const {
        return  !isnan(_m[0]) && !isnan(_m[1]) && !isnan(_m[2]) &&
                !isnan(_m[3]) && !isnan(_m[4]) && !isnan(_m[5]) &&
                !isnan(_m[6]) && !isnan(_m[7]) && !isnan(_m[8]);
    }
    
protected:
    real_t _m[9];
};

class Matrix4 {
public:
    Matrix4() {
        _m[ 0] = static_cast<real_t>(0.0); _m[ 1] = static_cast<real_t>(0.0); _m[ 2] = static_cast<real_t>(0.0); _m[ 3] = static_cast<real_t>(0.0);
        _m[ 4] = static_cast<real_t>(0.0); _m[ 5] = static_cast<real_t>(0.0); _m[ 6] = static_cast<real_t>(0.0); _m[ 7] = static_cast<real_t>(0.0);
        _m[ 8] = static_cast<real_t>(0.0); _m[ 9] = static_cast<real_t>(0.0); _m[10] = static_cast<real_t>(0.0); _m[11] = static_cast<real_t>(0.0);
        _m[12] = static_cast<real_t>(0.0); _m[13] = static_cast<real_t>(0.0); _m[14] = static_cast<real_t>(0.0); _m[15] = static_cast<real_t>(0.0);
    }
    Matrix4(float s) {
        _m[ 0] = s; _m[ 1] = s; _m[ 2] = s; _m[ 3] = s;
        _m[ 4] = s; _m[ 5] = s; _m[ 6] = s; _m[ 7] = s;
        _m[ 8] = s; _m[ 9] = s; _m[10] = s; _m[11] = s;
        _m[12] = s; _m[13] = s; _m[14] = s; _m[15] = s;
    }
    Matrix4(real_t m00, real_t m01, real_t m02, real_t m03,
            real_t m10, real_t m11, real_t m12, real_t m13,
            real_t m20, real_t m21, real_t m22, real_t m23,
            real_t m30, real_t m31, real_t m32, real_t m33) {
        _m[ 0] = m00; _m[ 1] = m01; _m[ 2] = m02; _m[ 3] = m03;
        _m[ 4] = m10; _m[ 5] = m11; _m[ 6] = m12; _m[ 7] = m13;
        _m[ 8] = m20; _m[ 9] = m21; _m[10] = m22; _m[11] = m23;
        _m[12] = m30; _m[13] = m31; _m[14] = m32; _m[15] = m33;
    }
    Matrix4(const Vector4 & r1, const Vector4 & r2, const Vector4 & r3, const Vector4 & r4) {
        _m[ 0] = r1[0]; _m[ 1] = r1[1]; _m[ 2] = r1[2]; _m[ 3] = r1[3];
        _m[ 4] = r2[0]; _m[ 5] = r2[1]; _m[ 6] = r2[2]; _m[ 7] = r2[3];
        _m[ 8] = r3[0]; _m[ 9] = r3[1]; _m[10] = r3[2]; _m[11] = r3[3];
        _m[12] = r4[0]; _m[13] = r4[1]; _m[14] = r4[2]; _m[15] = r4[3];
    }
    Matrix4(const real_t * m) {
        _m[ 0] = m[ 0]; _m[ 1] = m[ 1]; _m[ 2] = m[ 2]; _m[ 3] = m[ 3];
        _m[ 4] = m[ 4]; _m[ 5] = m[ 5]; _m[ 6] = m[ 6]; _m[ 7] = m[ 7];
        _m[ 8] = m[ 8]; _m[ 9] = m[ 9]; _m[10] = m[10]; _m[11] = m[11];
        _m[12] = m[12]; _m[13] = m[13]; _m[14] = m[14]; _m[15] = m[15];
    }
    Matrix4(const Matrix4 & m) {
        _m[ 0] = m._m[ 0]; _m[ 1] = m._m[ 1]; _m[ 2] = m._m[ 2]; _m[ 3] = m._m[ 3];
        _m[ 4] = m._m[ 4]; _m[ 5] = m._m[ 5]; _m[ 6] = m._m[ 6]; _m[ 7] = m._m[ 7];
        _m[ 8] = m._m[ 8]; _m[ 9] = m._m[ 9]; _m[10] = m._m[10]; _m[11] = m._m[11];
        _m[12] = m._m[12]; _m[13] = m._m[13]; _m[14] = m._m[14]; _m[15] = m._m[15];
    }
    Matrix4(const Matrix3 & m) {
        _m[ 0] = m[ 0]; _m[ 1] = m[ 1]; _m[ 2] = m[ 2]; _m[ 3] = static_cast<real_t>(0.0);
        _m[ 4] = m[ 3]; _m[ 5] = m[ 4]; _m[ 6] = m[ 5]; _m[ 7] = static_cast<real_t>(0.0);
        _m[ 8] = m[ 6]; _m[ 9] = m[ 7]; _m[10] = m[ 8]; _m[11] = static_cast<real_t>(0.0f);
        _m[12] = static_cast<real_t>(0.0);  _m[13] = static_cast<real_t>(0.0);  _m[14] = static_cast<real_t>(0.0);  _m[15] = static_cast<real_t>(1.0);
    }
    
    inline bool isZero() {
        return    _m[ 0]==static_cast<real_t>(0.0) && _m[ 1]==static_cast<real_t>(0.0) && _m[ 2]==static_cast<real_t>(0.0) && _m[ 3]==static_cast<real_t>(0.0) &&
        _m[ 4]==static_cast<real_t>(0.0) && _m[ 5]==static_cast<real_t>(0.0) && _m[ 6]==static_cast<real_t>(0.0) && _m[ 7]==static_cast<real_t>(0.0) &&
        _m[ 8]==static_cast<real_t>(0.0) && _m[ 9]==static_cast<real_t>(0.0) && _m[10]==static_cast<real_t>(0.0) && _m[11]==static_cast<real_t>(0.0) &&
        _m[12]==static_cast<real_t>(0.0) && _m[13]==static_cast<real_t>(0.0) && _m[14]==static_cast<real_t>(0.0) && _m[15]==static_cast<real_t>(0.0);
    }
    
    inline bool isIdentity() {
        return    _m[ 0]==static_cast<real_t>(1.0) && _m[ 1]==static_cast<real_t>(0.0) && _m[ 2]==static_cast<real_t>(0.0) && _m[ 3]==static_cast<real_t>(0.0)&&
        _m[ 4]==static_cast<real_t>(0.0) && _m[ 5]==static_cast<real_t>(1.0) && _m[ 6]==static_cast<real_t>(0.0) && _m[ 7]==static_cast<real_t>(0.0) &&
        _m[ 8]==static_cast<real_t>(0.0) && _m[ 9]==static_cast<real_t>(0.0) && _m[10]==static_cast<real_t>(1.0) && _m[11]==static_cast<real_t>(0.0) &&
        _m[12]==static_cast<real_t>(0.0) && _m[13]==static_cast<real_t>(0.0) && _m[14]==static_cast<real_t>(0.0) && _m[15]==static_cast<real_t>(1.0);
    }
    
    inline Matrix4 & identity() {
        (*this) = Matrix4::Identity();
        return *this;
    }
    
    static Vector4 Unproject(real_t x, real_t y, real_t depth, const Matrix4 & mvMat, const Matrix4 & pMat,const Viewport & viewport) {
        Matrix4 mvp(pMat);
        mvp.mult(mvMat);
        mvp.invert();
        
        Vector4 vin(((x - static_cast<real_t>(viewport.y())) / static_cast<real_t>(viewport.width())) * static_cast<real_t>(2.0) - static_cast<real_t>(1.0),
                    ((y - static_cast<real_t>(viewport.x())) / static_cast<real_t>(viewport.height())) * static_cast<real_t>(2.0) - static_cast<real_t>(1.0),
                    depth * static_cast<real_t>(2.0) - static_cast<real_t>(1.0),
                    static_cast<real_t>(1.0));
        
        Vector4 result = mvp.multVector(vin);
        if (result[3]==static_cast<real_t>(0.0)) {
            result.set(static_cast<real_t>(0.0));
        }
        else {
            result.set(result.x()/result.w(),
                       result.y()/result.w(),
                       result.z()/result.w(),
                       result.w()/result.w());
        }
        
        return result;
    }
    
    static Matrix4 Identity() {
        Matrix4 i;
        i._m[ 0] = static_cast<real_t>(1.0); i._m[ 1] = static_cast<real_t>(0.0); i._m[ 2] = static_cast<real_t>(0.0); i._m[ 3] = static_cast<real_t>(0.0);
        i._m[ 4] = static_cast<real_t>(0.0); i._m[ 5] = static_cast<real_t>(1.0); i._m[ 6] = static_cast<real_t>(0.0); i._m[ 7] = static_cast<real_t>(0.0);
        i._m[ 8] = static_cast<real_t>(0.0); i._m[ 9] = static_cast<real_t>(0.0); i._m[10] = static_cast<real_t>(1.0); i._m[11] = static_cast<real_t>(0.0);
        i._m[12] = static_cast<real_t>(0.0); i._m[13] = static_cast<real_t>(0.0); i._m[14] = static_cast<real_t>(0.0); i._m[15] = static_cast<real_t>(1.0);
        return i;
    }
    
    static Matrix4 Perspective(const Scalar & fovy, real_t aspect, real_t nearPlane, real_t farPlane) {
        return Matrix4::Perspective(trigonometry::radiansToDegrees(fovy.value()), aspect, nearPlane, farPlane);
    }
    
    static Matrix4 Perspective(real_t fovy, real_t aspect, real_t nearPlane, real_t farPlane) {
        real_t fovy2 = trigonometry::tan(fovy * static_cast<real_t>(kPi) / 360.0f) * nearPlane;
        real_t fovy2aspect = fovy2 * aspect;
        
        return Matrix4::Frustum(-fovy2aspect,fovy2aspect,-fovy2,fovy2,nearPlane,farPlane);
    }
    
    static Matrix4 Frustum(real_t left, real_t right, real_t bottom, real_t top, real_t nearPlane, real_t farPlane) {
        Matrix4 res;
        real_t A = right-left;
        real_t B = top-bottom;
        real_t C = farPlane-nearPlane;
        
        res.setRow(0, Vector4(nearPlane*static_cast<real_t>(2.0)/A,    static_cast<real_t>(0.0),    static_cast<real_t>(0.0),    static_cast<real_t>(0.0)));
        res.setRow(1, Vector4(static_cast<real_t>(0.0),    nearPlane*static_cast<real_t>(2.0)/B,    static_cast<real_t>(0.0),    static_cast<real_t>(0.0)));
        res.setRow(2, Vector4((right+left)/A,    (top+bottom)/B,    -(farPlane+nearPlane)/C,    -static_cast<real_t>(1.0)));
        res.setRow(3, Vector4(static_cast<real_t>(0.0),    static_cast<real_t>(0.0),    -(farPlane*nearPlane*static_cast<real_t>(2.0))/C,    static_cast<real_t>(0.0)));
        
        return res;
    }
    
    static Matrix4 Ortho(real_t left, real_t right, real_t bottom, real_t top, real_t nearPlane, real_t farPlane) {
        Matrix4 p;
        
        real_t m = right-left;
        real_t l = top-bottom;
        real_t k=farPlane-nearPlane;;
        
        p._m[0] = 2/m; p._m[1] = 0;   p._m[2] = 0;     p._m[3] = 0;
        p._m[4] = 0;   p._m[5] = 2/l; p._m[6] = 0;     p._m[7] = 0;
        p._m[8] = 0;   p._m[9] = 0;   p._m[10] = -2/k; p._m[11]=0;
        p._m[12]=-(left+right)/m; p._m[13] = -(top+bottom)/l; p._m[14] = -(farPlane+nearPlane)/k; p._m[15]=1;
        
        
        return p;
    }
    
    static Matrix4 LookAt(const Vector3 & p_eye, const Vector3 & p_center, const Vector3 & p_up) {
        Matrix4 result;
        Vector3 forward(p_center);
        forward.sub(p_eye);
        Vector3 up(p_up);
        
        forward.normalize();
        
        Vector3 side(forward);
        side = cross(side,up);
        up = side;
        up = cross(up,forward);
        
        result.identity();
        result.set00(side.x()); result.set10(side.y()); result.set20(side.z());
        result.set01(up.x()); result.set11(up.y()); result.set21(up.z());
        result.set02(-forward.x()); result.set12(-forward.y()); result.set22(-forward.z());
        result.setRow(3, Vector4(-p_eye.x(),-p_eye.y(),-p_eye.z(),1.0));
        
        return result;
    }
    
    static Matrix4 Translation(real_t x, real_t y, real_t z) {
        Matrix4 t;
        t._m[ 0] = 1.0f; t._m[ 1] = 0.0f; t._m[ 2] = 0.0f; t._m[ 3] = 0.0f;
        t._m[ 4] = 0.0f; t._m[ 5] = 1.0f; t._m[ 6] = 0.0f; t._m[ 7] = 0.0f;
        t._m[ 8] = 0.0f; t._m[ 9] = 0.0f; t._m[10] = 1.0f; t._m[11] = 0.0f;
        t._m[12] =    x; t._m[13] =    y; t._m[14] =    z; t._m[15] = 1.0f;
        return t;
    }
    
    static Matrix4 Translation(const Scalar & x, const Scalar & y, const Scalar & z) {
        Matrix4 t;
        t._m[ 0] = 1.0f; t._m[ 1] = 0.0f; t._m[ 2] = 0.0f; t._m[ 3] = 0.0f;
        t._m[ 4] = 0.0f; t._m[ 5] = 1.0f; t._m[ 6] = 0.0f; t._m[ 7] = 0.0f;
        t._m[ 8] = 0.0f; t._m[ 9] = 0.0f; t._m[10] = 1.0f; t._m[11] = 0.0f;
        t._m[12] = x.value(); t._m[13] = y.value(); t._m[14] = z.value(); t._m[15] = 1.0f;
        return t;
    }
    
    static Matrix4 Translation(const Vector3 & t) {
        return Translation(t.x(), t.y(), t.z());
    }
    
    static Matrix4 Rotation(const Scalar & alpha, real_t x, real_t y, real_t z) {
        return Rotation(alpha.value(), x, y, z);
    }
    
    static Matrix4 Rotation(real_t alpha, real_t x, real_t y, real_t z) {
        Vector3 axis(x,y,z);
        axis.normalize();
        Matrix4 rot;
        
        rot.identity();
        
        real_t cosAlpha = static_cast<real_t>(cosf(static_cast<float>(alpha)));
        real_t acosAlpha = static_cast<real_t>(1.0) - cosAlpha;
        real_t sinAlpha = static_cast<real_t>(sinf(static_cast<float>(alpha)));
        
        rot._m[0] = axis.x() * axis.x() * acosAlpha + cosAlpha;
        rot._m[1] = axis.x() * axis.y() * acosAlpha + axis.z() * sinAlpha;
        rot._m[2] = axis.x() * axis.z() * acosAlpha - axis.y() * sinAlpha;
        
        rot._m[4] = axis.y() * axis.x() * acosAlpha - axis.z() * sinAlpha;
        rot._m[5] = axis.y() * axis.y() * acosAlpha + cosAlpha;
        rot._m[6] = axis.y() * axis.z() * acosAlpha + axis.x() * sinAlpha;
        
        rot._m[8] = axis.z() * axis.x() * acosAlpha + axis.y() * sinAlpha;
        rot._m[9] = axis.z() * axis.y() * acosAlpha - axis.x() * sinAlpha;
        rot._m[10] = axis.z() * axis.z() * acosAlpha + cosAlpha;
        
        return rot;
    }
    
    static Matrix4 Scale(real_t x, real_t y, real_t z) {
        Matrix4 s;
        
        s._m[ 0] =    x; s._m[ 1] = static_cast<real_t>(0.0); s._m[ 2] = static_cast<real_t>(0.0); s._m[ 3] = static_cast<real_t>(0.0);
        s._m[ 4] = static_cast<real_t>(0.0); s._m[ 5] =    y; s._m[ 6] = static_cast<real_t>(0.0); s._m[ 7] = static_cast<real_t>(0.0);
        s._m[ 8] = static_cast<real_t>(0.0); s._m[ 9] = static_cast<real_t>(0.0); s._m[10] =    z; s._m[11] = static_cast<real_t>(0.0);
        s._m[12] = static_cast<real_t>(0.0); s._m[13] = static_cast<real_t>(0.0); s._m[14] = static_cast<real_t>(0.0); s._m[15] = static_cast<real_t>(1.0);
        
        return s;
    }
    
    static Matrix4 Scale(const Vector3 & s) {
        return Scale(s.x(), s.y(), s.z());
    }
    
    inline Matrix4 & perspective(float fovy, real_t aspect, real_t nearPlane, real_t farPlane) {
        *this = Matrix4::Perspective(fovy, aspect, nearPlane, farPlane);
        return *this;
    }
    
    inline Matrix4 & perspective(const Scalar & fovy, real_t aspect, real_t nearPlane, real_t farPlane) {
        *this = Matrix4::Perspective(fovy, aspect, nearPlane, farPlane);
        return *this;
    }
    
    inline Matrix4 & frustum(real_t left, real_t right, real_t bottom, real_t top, real_t nearPlane, real_t farPlane) {
        *this = Matrix4::Frustum(left, right, bottom, top, nearPlane, farPlane);
        return *this;
    }
    
    inline Matrix4 & ortho(real_t left, real_t right, real_t bottom, real_t top, real_t nearPlane, real_t farPlane) {
        *this = Matrix4::Ortho(left, right, bottom, top, nearPlane, farPlane);
        return *this;
    }
    
    inline Matrix4 & lookAt(const Vector3 & origin, const Vector3 & target, const Vector3 & up) {
        *this = Matrix4::LookAt(origin,target,up);
        return *this;
    }
    
    inline Matrix4 & translate(real_t x, real_t y, real_t z) {
        mult(Matrix4::Translation(x, y, z));
        return *this;
    }
    
    inline Matrix4 & translate(const Scalar & x, const Scalar & y, const Scalar & z) {
        mult(Matrix4::Translation(x, y, z));
        return *this;
    }
    
    inline Matrix4 & translate(const Vector3 & t) {
        mult(Matrix4::Translation(t));
        return *this;
    }
    
    inline Matrix4 & rotate(const Scalar & alpha, real_t x, real_t y, real_t z) {
        mult(Matrix4::Rotation(alpha.value(), x, y, z));
        return *this;
    }
    
    inline Matrix4 & rotate(real_t alpha, real_t x, real_t y, real_t z) {
        mult(Matrix4::Rotation(alpha, x, y, z));
        return *this;
    }
    
    inline Matrix4 & scale(real_t x, real_t y, real_t z) {
        mult(Matrix4::Scale(x, y, z));
        return *this;
    }
    
    inline Matrix4 & scale(const Vector3 & s) {
        mult(Matrix4::Scale(s));
        return *this;
    }
    
    inline const real_t * row(int i) const { return _m + (i*4); }
    inline void setRow(int i, const Vector4 & r) { _m[i*4]=r[0]; _m[i*4+1]=r[1]; _m[i*4+2]=r[2]; _m[i*4+3]=r[3]; }
    inline void set(real_t s) {
        _m[ 0]=s; _m[ 1]=s; _m[ 2]=s; _m[ 3]=s;
        _m[ 4]=s; _m[ 5]=s; _m[ 6]=s; _m[ 7]=s;
        _m[ 8]=s; _m[ 9]=s; _m[10]=s; _m[11]=s;
        _m[12]=s; _m[13]=s; _m[14]=s; _m[15]=s;
    }
    
    inline Matrix3 getMatrix3() const {
        Matrix3 res(_m[ 0],_m[ 1],_m[ 2],
                    _m[ 4],_m[ 5],_m[ 6],
                    _m[ 8],_m[ 9],_m[10]);
        return res;
    }
    
    inline const real_t * raw() const { return _m; }
    inline int length() const { return 15; }
    
    inline real_t operator[](int i) const { return _m[i]; }
    inline void operator=(const float * m) {
        _m[ 0] = m[ 0]; _m[ 1] = m[ 1]; _m[ 2] = m[ 2]; _m[ 3] = m[ 3];
        _m[ 4] = m[ 4]; _m[ 5] = m[ 5]; _m[ 6] = m[ 6]; _m[ 7] = m[ 7];
        _m[ 8] = m[ 8]; _m[ 9] = m[ 9]; _m[10] = m[10]; _m[11] = m[11];
        _m[12] = m[12]; _m[13] = m[13]; _m[14] = m[14]; _m[15] = m[15];
    }
    inline void operator=(const Matrix4 & m) {
        _m[ 0] = m._m[ 0]; _m[ 1] = m._m[ 1]; _m[ 2] = m._m[ 2]; _m[ 3] = m._m[ 3];
        _m[ 4] = m._m[ 4]; _m[ 5] = m._m[ 5]; _m[ 6] = m._m[ 6]; _m[ 7] = m._m[ 7];
        _m[ 8] = m._m[ 8]; _m[ 9] = m._m[ 9]; _m[10] = m._m[10]; _m[11] = m._m[11];
        _m[12] = m._m[12]; _m[13] = m._m[13]; _m[14] = m._m[14]; _m[15] = m._m[15];
    }
    inline void operator=(const Matrix3 & m) {
        _m[ 0] = m[ 0]; _m[ 1] = m[ 1]; _m[ 2] = m[ 2]; _m[ 3] = 0.0f;
        _m[ 4] = m[ 3]; _m[ 5] = m[ 4]; _m[ 6] = m[ 5]; _m[ 7] = 0.0f;
        _m[ 8] = m[ 6]; _m[ 9] = m[ 7]; _m[10] = m[ 8]; _m[11] = 0.0f;
        _m[12] = 0.0f;  _m[13] = 0.0f;  _m[14] = 0.0f;  _m[15] = 1.0f;
    }
    inline bool operator==(const Matrix4 & m) const {
        return    _m[ 0]==m._m[ 0] && _m[ 1]==m._m[ 1] && _m[ 2]==m._m[ 2] && _m[ 3]==m._m[ 3] &&
        _m[ 4]==m._m[ 4] && _m[ 5]==m._m[ 5] && _m[ 6]==m._m[ 6] && _m[ 7]==m._m[ 7] &&
        _m[ 8]==m._m[ 8] && _m[ 9]==m._m[ 9] && _m[10]==m._m[10] && _m[11]==m._m[11] &&
        _m[12]==m._m[12] && _m[13]==m._m[13] && _m[14]==m._m[14] && _m[15]==m._m[15];
    }
    inline bool operator!=(const Matrix4 & m) const {
        return    _m[0]  != m._m[0]  || _m[1]  != m._m[1]  || _m[2]  != m._m[2]  || _m[3]  != m._m[3]  ||
        _m[4]  != m._m[4]  || _m[5]  != m._m[5]  || _m[6]  != m._m[6]  || _m[7]  != m._m[7]  ||
        _m[8]  != m._m[8]  || _m[9]  != m._m[9]  || _m[10] != m._m[10] || _m[11] != m._m[11] ||
        _m[12] != m._m[12] || _m[13] != m._m[13] || _m[14] != m._m[14] || _m[15] != m._m[15];
    }
    
    inline std::string toString() const {
        std::stringstream ss;
        
        ss    << '[' << _m[ 0] << ',' << _m[ 1] << ',' << _m[ 2] << ',' << _m[ 3] << ',' << std::endl << ' '
        << _m[ 4] << ',' << _m[ 5] << ',' << _m[ 6] << ',' << _m[ 7] << ',' << std::endl << ' '
        << _m[ 8] << ',' << _m[ 9] << ',' << _m[10] << ',' << _m[11] << ',' << std::endl << ' '
        << _m[12] << ',' << _m[13] << ',' << _m[14] << ',' << _m[15] << ']';
        
        return ss.str();
    }
    
    inline Matrix4 & mult(const Matrix4 & rMatrix) {
        //        _m[ 0] _m[ 1] _m[ 2] _m[ 3]         rm[ 0] rm[ 1] rm[ 2] rm[ 3]
        //        _m[ 4] _m[ 5] _m[ 6] _m[ 7]            rm[ 4] rm[ 5] rm[ 6] rm[ 7]
        //        _m[ 8] _m[ 9] _m[10] _m[11]            rm[ 8] rm[ 9] rm[10] rm[11]
        //        _m[12] _m[13] _m[14] _m[15]            rm[12] rm[13] rm[14] rm[15]
        const real_t * rm = _m;
        const real_t * lm = rMatrix.raw();
        Matrix4 res;
        
        res._m[ 0] = lm[ 0] * rm[ 0] + lm[ 1] * rm[ 4] + lm[ 2] * rm[ 8] + lm[ 3] * rm[12];
        res._m[ 1] = lm[ 0] * rm[ 1] + lm[ 1] * rm[ 5] + lm[ 2] * rm[ 9] + lm[ 3] * rm[13];
        res._m[ 2] = lm[ 0] * rm[ 2] + lm[ 1] * rm[ 6] + lm[ 2] * rm[10] + lm[ 3] * rm[14];
        res._m[ 3] = lm[ 0] * rm[ 3] + lm[ 1] * rm[ 7] + lm[ 2] * rm[11] + lm[ 3] * rm[15];
        
        res._m[ 4] = lm[ 4] * rm[ 0] + lm[ 5] * rm[ 4] + lm[ 6] * rm[ 8] + lm[ 7] * rm[12];
        res._m[ 5] = lm[ 4] * rm[ 1] + lm[ 5] * rm[ 5] + lm[ 6] * rm[ 9] + lm[ 7] * rm[13];
        res._m[ 6] = lm[ 4] * rm[ 2] + lm[ 5] * rm[ 6] + lm[ 6] * rm[10] + lm[ 7] * rm[14];
        res._m[ 7] = lm[ 4] * rm[ 3] + lm[ 5] * rm[ 7] + lm[ 6] * rm[11] + lm[ 7] * rm[15];
        
        res._m[ 8] = lm[ 8] * rm[ 0] + lm[ 9] * rm[ 4] + lm[10] * rm[ 8] + lm[11] * rm[12];
        res._m[ 9] = lm[ 8] * rm[ 1] + lm[ 9] * rm[ 5] + lm[10] * rm[ 9] + lm[11] * rm[13];
        res._m[10] = lm[ 8] * rm[ 2] + lm[ 9] * rm[ 6] + lm[10] * rm[10] + lm[11] * rm[14];
        res._m[11] = lm[ 8] * rm[ 3] + lm[ 9] * rm[ 7] + lm[10] * rm[11] + lm[11] * rm[15];
        
        res._m[12] = lm[12] * rm[ 0] + lm[13] * rm[ 4] + lm[14] * rm[ 8] + lm[15] * rm[12];
        res._m[13] = lm[12] * rm[ 1] + lm[13] * rm[ 5] + lm[14] * rm[ 9] + lm[15] * rm[13];
        res._m[14] = lm[12] * rm[ 2] + lm[13] * rm[ 6] + lm[14] * rm[10] + lm[15] * rm[14];
        res._m[15] = lm[12] * rm[ 3] + lm[13] * rm[ 7] + lm[14] * rm[11] + lm[15] * rm[15];
        
        (*this) = res;
        return *this;
    }
    
    inline Matrix4 & mult(float s) {
        _m[ 0] *= s; _m[ 1] *= s; _m[ 2] *= s; _m[ 3] *= s;
        _m[ 4] *= s; _m[ 5] *= s; _m[ 6] *= s; _m[ 7] *= s;
        _m[ 8] *= s; _m[ 9] *= s; _m[10] *= s; _m[11] *= s;
        _m[12] *= s; _m[13] *= s; _m[14] *= s; _m[15] *= s;
        return *this;
    }
    
    inline Vector4 multVector(const Vector3 & vec) const {
        Vector4 res;
        real_t x=vec[0]; real_t y=vec[1]; real_t z=vec[2]; real_t w=1.0f;
        
        res.x(_m[0]*x + _m[4]*y + _m[8] *z + _m[12] * w);
        res.y(_m[1]*x + _m[5]*y + _m[9] *z + _m[13] * w);
        res.z(_m[2]*x + _m[6]*y + _m[10]*z + _m[14] * w);
        res.w(_m[3]*x + _m[7]*y + _m[11]*z + _m[15] * w);
        
        return res;
    }
    
    inline Vector4 multVector(const Vector4 & vec) const {
        Vector4 res;
        real_t x=vec[0]; real_t y=vec[1]; real_t z=vec[2]; real_t w=vec[3];
        
        res.x(_m[0]*x + _m[4]*y + _m[8] *z + _m[12] * w);
        res.y(_m[1]*x + _m[5]*y + _m[9] *z + _m[13] * w);
        res.z(_m[2]*x + _m[6]*y + _m[10]*z + _m[14] * w);
        res.w(_m[3]*x + _m[7]*y + _m[11]*z + _m[15] * w);
        
        return res;
    }
    
    inline Matrix4 & invert() {
        real_t inv[16];
        
        inv[0] = _m[5] * _m[10] * _m[15] - _m[5] * _m[11] * _m[14] - _m[9] * _m[6] * _m[15] + _m[9] * _m[7] * _m[14] + _m[13] * _m[6] * _m[11] - _m[13] * _m[7] * _m[10];
        inv[4] = -_m[4] * _m[10] * _m[15] + _m[4] * _m[11] * _m[14] + _m[8] * _m[6] * _m[15] - _m[8] * _m[7] * _m[14] - _m[12] * _m[6] * _m[11] + _m[12] * _m[7] * _m[10];
        inv[8] = _m[4] * _m[9] * _m[15] - _m[4] * _m[11] * _m[13] - _m[8] * _m[5] * _m[15] + _m[8] * _m[7] * _m[13] + _m[12] * _m[5] * _m[11] - _m[12] * _m[7] * _m[9];
        inv[12] = -_m[4] * _m[9] * _m[14] + _m[4] * _m[10] * _m[13] + _m[8] * _m[5] * _m[14] - _m[8] * _m[6] * _m[13] - _m[12] * _m[5] * _m[10] + _m[12] * _m[6] * _m[9];
        inv[1] = -_m[1] * _m[10] * _m[15] + _m[1] * _m[11] * _m[14] + _m[9] * _m[2] * _m[15] - _m[9] * _m[3] * _m[14] - _m[13] * _m[2] * _m[11] + _m[13] * _m[3] * _m[10];
        inv[5] = _m[0] * _m[10] * _m[15] - _m[0] * _m[11] * _m[14] - _m[8] * _m[2] * _m[15] + _m[8] * _m[3] * _m[14] + _m[12] * _m[2] * _m[11] - _m[12] * _m[3] * _m[10];
        inv[9] = -_m[0] * _m[9] * _m[15] + _m[0] * _m[11] * _m[13] + _m[8] * _m[1] * _m[15] - _m[8] * _m[3] * _m[13] - _m[12] * _m[1] * _m[11] + _m[12] * _m[3] * _m[9];
        inv[13] = _m[0] * _m[9] * _m[14] - _m[0] * _m[10] * _m[13] - _m[8] * _m[1] * _m[14] + _m[8] * _m[2] * _m[13] + _m[12] * _m[1] * _m[10] - _m[12] * _m[2] * _m[9];
        inv[2] = _m[1] * _m[6] * _m[15] - _m[1] * _m[7] * _m[14] - _m[5] * _m[2] * _m[15] + _m[5] * _m[3] * _m[14] + _m[13] * _m[2] * _m[7] - _m[13] * _m[3] * _m[6];
        inv[6] = -_m[0] * _m[6] * _m[15] + _m[0] * _m[7] * _m[14] + _m[4] * _m[2] * _m[15] - _m[4] * _m[3] * _m[14] - _m[12] * _m[2] * _m[7] + _m[12] * _m[3] * _m[6];
        inv[10] = _m[0] * _m[5] * _m[15] - _m[0] * _m[7] * _m[13] - _m[4] * _m[1] * _m[15] + _m[4] * _m[3] * _m[13] + _m[12] * _m[1] * _m[7] - _m[12] * _m[3] * _m[5];
        inv[14] = -_m[0] * _m[5] * _m[14] + _m[0] * _m[6] * _m[13] + _m[4] * _m[1] * _m[14] - _m[4] * _m[2] * _m[13] - _m[12] * _m[1] * _m[6] + _m[12] * _m[2] * _m[5];
        inv[3] = -_m[1] * _m[6] * _m[11] + _m[1] * _m[7] * _m[10] + _m[5] * _m[2] * _m[11] - _m[5] * _m[3] * _m[10] - _m[9] * _m[2] * _m[7] + _m[9] * _m[3] * _m[6];
        inv[7] = _m[0] * _m[6] * _m[11] - _m[0] * _m[7] * _m[10] - _m[4] * _m[2] * _m[11] + _m[4] * _m[3] * _m[10] + _m[8] * _m[2] * _m[7] - _m[8] * _m[3] * _m[6];
        inv[11] = -_m[0] * _m[5] * _m[11] + _m[0] * _m[7] * _m[9] + _m[4] * _m[1] * _m[11] - _m[4] * _m[3] * _m[9] - _m[8] * _m[1] * _m[7] + _m[8] * _m[3] * _m[5];
        inv[15] = _m[0] * _m[5] * _m[10] - _m[0] * _m[6] * _m[9] - _m[4] * _m[1] * _m[10] + _m[4] * _m[2] * _m[9] + _m[8] * _m[1] * _m[6] - _m[8] * _m[2] * _m[5];
        
        real_t det = _m[0] * inv[0] + _m[1] * inv[4] + _m[2] * inv[8] + _m[3] * inv[12];
        
        if (det==0.0f) {
            set(0.0f);
        }
        else {
            det = 1.0f / det;
            
            _m[0]  = inv[0]  * det;
            _m[1]  = inv[1]  * det;
            _m[2]  = inv[2]  * det;
            _m[3]  = inv[3]  * det;
            _m[4]  = inv[4]  * det;
            _m[5]  = inv[5]  * det;
            _m[6]  = inv[6]  * det;
            _m[7]  = inv[7]  * det;
            _m[8]  = inv[8]  * det;
            _m[9]  = inv[9]  * det;
            _m[10] = inv[10] * det;
            _m[11] = inv[11] * det;
            _m[12] = inv[12] * det;
            _m[13] = inv[13] * det;
            _m[14] = inv[14] * det;
            _m[15] = inv[15] * det;
        }
        
        return *this;
    }
    
    inline Matrix4 & transpose() {
        Vector4 r0(_m[0], _m[4], _m[ 8], _m[12]);
        Vector4 r1(_m[1], _m[5], _m[ 9], _m[13]);
        Vector4 r2(_m[2], _m[6], _m[10], _m[14]);
        Vector4 r3(_m[3], _m[7], _m[11], _m[15]);
        
        setRow(0, r0);
        setRow(1, r1);
        setRow(2, r2);
        setRow(3, r3);
        return *this;
    }
    
    inline Matrix4 & setPosition(real_t x, real_t y, real_t z) {
        _m[12] = x; _m[13] = y; _m[14] = z;
        return *this;
    }
    
    inline Matrix4 & setPosition(const Vector3 & pos) {
        _m[12] = pos.x(); _m[13] = pos.y(); _m[14] = pos.z();
        return *this;
    }
    
    inline Matrix4 & setPosition(const Scalar & x, const Scalar & y, const Scalar & z) {
        _m[12] = x.value(); _m[13] = y.value(); _m[14] = z.value();
        return *this;
    }
    
    inline Vector3 position() const {
        return Vector3(_m[12], _m[13], _m[14]);
        
    }
    
    inline Matrix4 rotation() const {
        return Matrix4(
                       _m[0], _m[1], _m[ 2], static_cast<real_t>(0.0),
                       _m[4], _m[5], _m[ 6], static_cast<real_t>(0.0),
                       _m[8], _m[9], _m[10], static_cast<real_t>(0.0),
                       static_cast<real_t>(0.0),  static_cast<real_t>(0.0),  static_cast<real_t>(0.0),   static_cast<real_t>(1.0)
                    );
    }
    
    inline Vector3 scale() const {
        return Vector3(
                       Vector3(_m[0], _m[1], _m[ 2]).magnitude(),
                       Vector3(_m[4], _m[5], _m[ 6]).magnitude(),
                       Vector3(_m[8], _m[9], _m[10]).magnitude()
                       );
    }
    
    inline void resetScale() {
        Vector3 rx(_m[0], _m[1], _m[ 2]);
        Vector3 ry(_m[4], _m[5], _m[ 6]);
        Vector3 rz(_m[8], _m[9], _m[10]);
        rx.normalize();
        ry.normalize();
        rz.normalize();
        
        set00(rx.x()); set01(rx.y()); set02(rx.z());
        set10(ry.x()); set11(ry.y()); set12(ry.z());
        set20(rz.x()); set21(rz.y()); set22(rz.z());
    }
    
    inline void setScale(Vector3 s) {
        resetScale();
        scale(s.x(), s.y(), s.z());
    }
    
    inline Matrix4 resetRotation() {
        Vector3 scale = this->scale();
        _m[0] = scale.x(); _m[1] = static_cast<real_t>(0.0); _m[ 2] = static_cast<real_t>(0.0);
        _m[4] = 0.0f; _m[5] = scale.y(); _m[ 6] = static_cast<real_t>(0.0);
        _m[8] = 0.0f; _m[9] = static_cast<real_t>(0.0); _m[10] = scale.z();
        return *this;
    }
    
    inline Vector3 transformDirection(const Vector3 & dir) {
        Vector3 direction = dir;
        Matrix4 trx = *this;
        trx.setRow(3, Vector4(0.0f, 0.0f, 0.0f, 1.0f));
        direction = trx.multVector(direction).xyz();
        direction.normalize();
        return direction;
    }
    
    inline Vector3 forwardVector() {
        return transformDirection(Vector3(0.0f, 0.0f, 1.0f));
    }
    
    inline Vector3 rightVector() {
        return transformDirection(Vector3(1.0f, 0.0f, 0.0f));
    }
    
    inline Vector3 upVector() {
        return transformDirection(Vector3(0.0f, 1.0f, 0.0f));
    }
    
    inline Vector3 backwardVector() {
        return transformDirection(Vector3(0.0f, 0.0f, -1.0f));
    }
    
    inline Vector3 leftVector() {
        return transformDirection(Vector3(-1.0f, 0.0f, 0.0f));
    }
    
    inline Vector3 downVector() {
        return transformDirection(Vector3(0.0f, -1.0f, 0.0f));
    }
    
    inline Vector3 eulerAngles() const {
        real_t rx = trigonometry::atan2(get12(), get22());
        real_t c2 = sqrt(get00() * get00() + get01() * get01());
        real_t ry = trigonometry::atan2(-1.0f * get02(),c2);
        real_t s1 = trigonometry::sin(rx);
        real_t c1 = trigonometry::cos(rx);
        real_t rz = atan2(s1 * get20() - c1 * get10(), c1 * get11() - s1 * get21());
        return Vector3(rx,ry,rz);
    }
    
    inline void set00(real_t v) { _m[0] = v; }
    inline void set01(real_t v) { _m[1] = v; }
    inline void set02(real_t v) { _m[2] = v; }
    inline void set03(real_t v) { _m[3] = v; }
    
    inline void set10(real_t v) { _m[4] = v; }
    inline void set11(real_t v) { _m[5] = v; }
    inline void set12(real_t v) { _m[6] = v; }
    inline void set13(real_t v) { _m[7] = v; }
    
    inline void set20(real_t v) { _m[8] = v; }
    inline void set21(real_t v) { _m[9] = v; }
    inline void set22(real_t v) { _m[10] = v; }
    inline void set23(real_t v) { _m[11] = v; }
    
    inline void set30(real_t v) { _m[12] = v; }
    inline void set31(real_t v) { _m[13] = v; }
    inline void set32(real_t v) { _m[14] = v; }
    inline void set33(real_t v) { _m[15] = v; }
    
    inline real_t get00() const { return _m[0]; }
    inline real_t get01() const { return _m[1]; }
    inline real_t get02() const { return _m[2]; }
    inline real_t get03() const { return _m[3]; }
    
    inline real_t get10() const { return _m[4]; }
    inline real_t get11() const { return _m[5]; }
    inline real_t get12() const { return _m[6]; }
    inline real_t get13() const { return _m[7]; }
    
    inline real_t get20() const { return _m[8]; }
    inline real_t get21() const { return _m[9]; }
    inline real_t get22() const { return _m[10]; }
    inline real_t get23() const { return _m[11]; }
    
    inline real_t get30() const { return _m[12]; }
    inline real_t get31() const { return _m[13]; }
    inline real_t get32() const { return _m[14]; }
    inline real_t get33() const { return _m[15]; }
    
    inline bool isNan() const {
        return    isnan(_m[ 0]) || isnan(_m[ 1]) || isnan(_m[ 2]) || isnan(_m[ 3]) ||
        isnan(_m[ 4]) || isnan(_m[ 5]) || isnan(_m[ 6]) || isnan(_m[ 7]) ||
        isnan(_m[ 8]) || isnan(_m[ 9]) || isnan(_m[10]) || isnan(_m[11]) ||
        isnan(_m[12]) || isnan(_m[13]) || isnan(_m[14]) || isnan(_m[15]);
    }
    
    void projectionValues(real_t & /* left */, real_t & /* right */, real_t & /* bottom */, real_t & /* top */, real_t & /* near */, real_t & /* far */) {
        
    }
    
    void orthoValues(real_t & left, real_t & right, real_t & bottom, real_t & top, real_t & near, real_t & far) {
        near   =  (1+get23())/get22();
        far    = -(1-get23())/get22();
        bottom =  (1-get13())/get11();
        top    = -(1+get13())/get11();
        left   = -(1+get03())/get00();
        right  =  (1-get03())/get00();
    }
    
    void perspectiveValues(real_t & left, real_t & right, real_t & bottom, real_t & top, real_t & near, real_t & far) {
        near   = get23()/(get22()-1);
        far    = get23()/(get22()+1);
        bottom = near * (get12()-1)/get11();
        top    = near * (get12()+1)/get11();
        left   = near * (get02()-1)/get00();
        right  = near * (get02()+1)/get00();
    }
    
protected:
    real_t _m[16];
};



}	// math
}	// bg


#endif
