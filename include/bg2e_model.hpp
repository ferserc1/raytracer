#ifndef _BG2EMODEL_HPP_
#define _BG2EMODEL_HPP_

#include <mesh.hpp>

#include <model.hpp>

class Bg2eModel {
public:
	static Hitable * Load(const std::string & path, const bg::math::Matrix4 & transform);
	static Hitable * Load(const Model * model);
};

class Bg2eScene {
public:
    static Hitable * Load(const std::string & path, const std::string & drawableName, Model *& model);
};

#endif
