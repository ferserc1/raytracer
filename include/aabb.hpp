#ifndef AABB_HPP_
#define AABB_HPP_

#include <ray.hpp>

inline real_t ffmin(real_t a, real_t b) { return a < b ? a : b; }
inline real_t ffmax(real_t a, real_t b) { return a > b ? a : b; }

class AABB {
public:
	static AABB SurroundingBox(const AABB & box0, const AABB & box1);

	AABB();
	AABB(const bg::math::Vector3 & a, const bg::math::Vector3 & b);

	inline const bg::math::Vector3 & min() const { return _min; }
	inline const bg::math::Vector3 & max() const { return _max; }

	bool hit(const Ray & r, real_t tmin, real_t tmax) const;

protected:
	bg::math::Vector3 _min;
	bg::math::Vector3 _max;
};
#endif
