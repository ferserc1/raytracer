
#ifndef _IMAGE_HPP_
#define _IMAGE_HPP_

#include <string>

class Image {
public:
	static Image * Load(const std::string & path);

	void save(const std::string & path);

	void loadFromFile(const std::string & path);

	void release();

	inline unsigned char * data() { return _data; }
	inline const unsigned char * data() const { return _data; }
	
	void setData(unsigned char * data, int w, int h, int channels);

	inline int width() const { return _width; }
	inline int height() const { return _height; }
	inline int channels() const { return _channels; }

	inline bool valid() const { return _width != 0 && _height != 0 && _channels != 0 && _data!=nullptr; }

protected:
	unsigned char * _data = nullptr;
	int _width = 0;
	int _height = 0;
	int _channels = 0;
};


#endif