#ifndef _SPHERE_H_
#define _SPHERE_H_

#include <hitable.hpp>
#include <material.hpp>

class Sphere : public Hitable {
public:
	Sphere() :_material(new Lambertian(bg::math::Vector3(0.5, 0.5, 0.5))) {}
	Sphere(const bg::math::Vector3 & center, real_t radius, Material * mat) :_center(center), _radius(radius), _material(mat) {}
	virtual ~Sphere() {
		if (_material) {
			delete _material;
			_material = nullptr;
		}
	}
	virtual bool hit(const Ray & ray, real_t tmin, real_t tmax, HitRecord & rec) const {
		using namespace bg::math;
		Vector3 oc = ray.origin() - center();
		real_t a = dot(ray.direction(), ray.direction());
		real_t b = dot(oc, ray.direction());
		real_t c = dot(oc, oc) - radius() * radius();
		real_t discriminant = b * b - a * c;
		rec.materialPtr = material();
		if (discriminant > 0.0f) {
			real_t temp = (-b - bg::math::sqrt(b * b - a * c)) / a;
			if (temp<tmax && temp>tmin) {
				rec.distance = temp;
				rec.point = ray.pointAtDistance(rec.distance);
				rec.normal = (rec.point - center()) / radius();
				rec.normal.normalize();
				return true;
			}
			temp = (-b + bg::math::sqrt(b * b - a * c)) / a;
			if (temp<tmax && temp>tmin) {
				rec.distance = temp;
				rec.point = ray.pointAtDistance(rec.distance);
				rec.normal = (rec.point - center()) / radius();
				rec.normal.normalize();
				return true;
			}
		}
		return false;
	}

	virtual bool boundingBox(float t0, float t1, AABB & box) const {
		using namespace bg::math;
		box = AABB(center() - Vector3(radius(), radius(), radius()), center() + Vector3(radius(), radius(), radius()));
		return true;
	}

	inline const bg::math::Vector3 & center() const { return _center; }
	inline bg::math::Vector3 & center() { return _center; }
	inline void setCenter(const bg::math::Vector3 & c) { _center = c; }
	inline real_t radius() const { return _radius; }
	inline void setRadius(real_t r) { _radius = r; }
	inline Material * material() { return _material; }
	inline const Material * material() const { return _material; }
	inline void setMaterial(Material * mat) {
		if (_material) {
			delete _material;
			_material = nullptr;
		}
		_material = mat;
	}

protected:
	bg::math::Vector3 _center;
	real_t _radius;
	Material * _material;
};

class MovingSphere : public Hitable {
public:
	MovingSphere() {}
	MovingSphere(const bg::math::Vector3 & cen0, const bg::math::Vector3 & cen1, real_t t0, real_t t1, real_t radius, Material * mat)
		:_center0(cen0), _center1(cen1), _time0(t0), _time1(t1), _radius(radius), _material(mat) {}

	virtual bool hit(const Ray & ray, real_t tmin, real_t tmax, HitRecord & rec) const {
		using namespace bg::math;
		Vector3 oc = ray.origin() - center(ray.time());
		real_t a = dot(ray.direction(), ray.direction());
		real_t b = dot(oc, ray.direction());
		real_t c = dot(oc, oc) - radius() * radius();
		real_t discriminant = b * b - a * c;
		rec.materialPtr = material();
		if (discriminant > 0.0f) {
			real_t temp = (-b - bg::math::sqrt(b * b - a * c)) / a;
			if (temp<tmax && temp>tmin) {
				rec.distance = temp;
				rec.point = ray.pointAtDistance(rec.distance);
				rec.normal = (rec.point - center(ray.time())) / radius();
				return true;
			}
			temp = (-b + bg::math::sqrt(b * b - a * c)) / a;
			if (temp<tmax && temp>tmin) {
				rec.distance = temp;
				rec.point = ray.pointAtDistance(rec.distance);
				rec.normal = (rec.point - center(ray.time())) / radius();
				return true;
			}
		}
		return false;
	}

	virtual bool boundingBox(float t0, float t1, AABB & box) const {
		using namespace bg::math;
		box = AABB::SurroundingBox(
			AABB(center(t0) - Vector3(radius(), radius(), radius()), center(t0) + Vector3(radius(), radius(), radius())),
			AABB(center(t1) - Vector3(radius(), radius(), radius()), center(t1) + Vector3(radius(), radius(), radius()))
		);
		return true;
	}

	bg::math::Vector3 center(real_t time) const {
		return _center0 + ((time - _time0) / (_time1 - _time0)) * (_center1 - _center0);
	}

	inline real_t radius() const { return _radius; }
	inline Material * material() { return _material; }
	inline const Material * material() const { return _material; }

protected:
	bg::math::Vector3 _center0, _center1;
	real_t _time0, _time1;
	real_t _radius;
	Material * _material;
};

#endif
