#ifndef _UV_MAP_HPP_
#define _UV_MAP_HPP_

#include <math_tools.hpp>
#include <hitable.hpp>
#include <frame.hpp>
#include <renderer.hpp>

#include <vector>

struct Triangle {
	bg::math::Vector3 v0;
	bg::math::Vector3 v1;
	bg::math::Vector3 v2;

	bg::math::Vector2 t0;
	bg::math::Vector2 t1;
	bg::math::Vector2 t2;
};


class UVMap {
public:
	typedef std::vector<Triangle> TriangleVector;

	inline const TriangleVector & triangles() const { return _triangles; }
	inline TriangleVector & triangles() { return _triangles; }

	void clear();

	inline void addVertex(const bg::math::Vector3 & v) { _vertex.push_back(v); }
	inline void addUV(const bg::math::Vector2 & v) { _uv.push_back(v); }
	inline void addTriangle(uint32_t i0, uint32_t i1, uint32_t i2) {
		_triangles.push_back({ _vertex[i0], _vertex[i1], _vertex[i2], _uv[i0], _uv[i1], _uv[i2], });
	}

	// Save the UV map triangle layout to an image file
	void saveMap(const std::string & path, Renderer & renderer, Hitable * world, int blurRadius, Frame & frame) const;
	
protected:
	TriangleVector _triangles;

	std::vector<bg::math::Vector3> _vertex;
	std::vector<bg::math::Vector2> _uv;

	bool getPoint(const Frame & frame, int x, int y, bg::math::Vector3 & point, bg::math::Vector3 & normal) const;
	void renderBucket(Frame & frame, Hitable * world, int left, int bottom, int width, int height) const;

};

#endif
