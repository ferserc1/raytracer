#ifndef _CAMERA_H_
#define _CAMERA_H_

#include <ray.hpp>

using namespace bg::math;

inline Vector3 randomInUnitDisk() {
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<> dis(0.0f, 1.0f);
	Vector3 p;
	do {
		p = 2.0 * Vector3(dis(gen), dis(gen), 0.0) - Vector3(1.0, 1.0, 0.0);
	} while (dot(p, p) >= 1.0);
	return p;
}

class Camera {
public:
	Camera()
		:_origin(0.0f,0.0f,0.0f)
		,_lowerLeftCorner(-2.0f, -1.0f, -1.0f)
		,_horizontal(4.0f, 0.0f, 0.0f)
		,_vertical(0.0f, 2.0f, 0.0f)
	{
	}

	Camera(const Vector3 & lookFrom, const Vector3 & lookAt, const Vector3 & vUp, real_t vfov, real_t aspect, real_t aperture, real_t focusDist, real_t t0, real_t t1) {
		using namespace bg::math;
		_lensRadius = aperture / 2.0;
		real_t theta = vfov * kPiOver180;
		real_t halfHeight = trigonometry::tan(theta / 2.0);
		real_t halfWidth = aspect * halfHeight;
		_origin = lookFrom;
		_w = unitVector(lookFrom - lookAt);
		_u = unitVector(cross(vUp, _w));
		_v = cross(_w, _u);

		_lowerLeftCorner = _origin - halfWidth * focusDist * _u - halfHeight * focusDist * _v - focusDist * _w;
		_horizontal = 2.0 * halfWidth * focusDist * _u;
		_vertical = 2.0 * halfHeight * focusDist * _v;
		_time0 = t0;
		_time1 = t1;
	}

	Camera(const Vector3 & origin, const Vector3 & lowLeft, const Vector3 & horizontal, const Vector3 & vertical)
		:_origin(origin)
		,_lowerLeftCorner(lowLeft)
		,_horizontal(horizontal)
		,_vertical(vertical)
		,_time0(0)
		,_time1(0)
		,_lensRadius(0.0)
	{
	}

	Ray getRay(real_t s, real_t t) {
		Vector3 rd = _lensRadius * randomInUnitDisk();
		Vector3 offset = _u * rd.x() + _v * rd.y();
		real_t time = _time0 + bg::math::random() * (_time1 - _time0);
		return Ray(_origin + offset, _lowerLeftCorner + s * _horizontal + t * _vertical - _origin - offset, time);
	}

	inline const bg::math::Vector3 & origin() const { return _origin; }
	inline const bg::math::Vector3 & lowerLeftCorner() const { return _lowerLeftCorner; }
	inline const bg::math::Vector3 & horizontal() const { return _horizontal; }
	inline const bg::math::Vector3 & vertical() const { return _vertical; }
	inline bg::math::Vector3 & origin() { return _origin; }
	inline bg::math::Vector3 & lowerLeftCorner() { return _lowerLeftCorner; }
	inline bg::math::Vector3 & horizontal() { return _horizontal; }
	inline bg::math::Vector3 & vertical() { return _vertical; }
	inline void setOrigin(const bg::math::Vector3 & v) { _origin = v; }
	inline void setLowerLeftCorner(const bg::math::Vector3 & v) { _lowerLeftCorner = v; }
	inline void setHorizontal(const bg::math::Vector3 & v) { _horizontal = v; }
	inline void setVertical(const bg::math::Vector3 & v) { _vertical = v; }
	inline const bg::math::Vector3 & u() const { return _u; }
	inline const bg::math::Vector3 & v() const { return _v; }
	inline const bg::math::Vector3 & w() const { return _w; }
	inline const real_t lensRadius() const { return _lensRadius; }
	inline const real_t time0() const { return _time0; }
	inline const real_t time1() const { return _time1; }

protected:
	bg::math::Vector3 _origin;
	bg::math::Vector3 _lowerLeftCorner;
	bg::math::Vector3 _horizontal;
	bg::math::Vector3 _vertical;
	Vector3 _u, _v, _w;
	real_t _lensRadius;
	real_t _time0;
	real_t _time1;
};

#endif
