#ifndef _HITABLE_LIST_HPP_
#define _HITABLE_LIST_HPP_

#include <hitable.hpp>

#include <vector>
#include <memory>

class HitableList : public Hitable {
public:
	typedef std::vector<Hitable*> HitableVector;

	HitableList() {}
	virtual ~HitableList() { _hitableList.clear(); }

	inline void addItem(Hitable * hitable) {
		_hitableList.push_back(hitable);
	}

	bool hit(const Ray & ray, real_t tmin, real_t tmax, HitRecord & rec) const {
		HitRecord tmpRec;
		bool hitAnything = false;
		real_t closestSoFar = tmax;
		for (const auto & hitable : _hitableList) {
			if (hitable->hit(ray, tmin, closestSoFar, tmpRec)) {
				hitAnything = true;
				closestSoFar = tmpRec.distance;
				rec = tmpRec;
			}
		}
		return hitAnything;
	}

	bool boundingBox(float t0, float t1, AABB & box) const {
		if (_hitableList.size() < 1) return false;
		AABB tempBox;
		bool firstTrue = _hitableList.front()->boundingBox(t0, t1, tempBox);
		if (!firstTrue) {
			return false;
		}
		else {
			box = tempBox;
		}
		for (const auto & hitable : _hitableList) {
			if (hitable->boundingBox(t0, t1, tempBox)) {
				box = AABB::SurroundingBox(box, tempBox);
			}
			else {
				return false;
			}
		}
		return true;
	}

	inline const HitableVector & hitableList() const { return _hitableList; }
	inline HitableVector & hitableList() { return _hitableList; }

protected:
	HitableVector _hitableList;
};

#endif
