#ifndef _HITABLE_HPP_
#define _HITABLE_HPP_

#include <ray.hpp>
#include <aabb.hpp>

class Material;

struct HitRecord {
	real_t distance;
	bg::math::Vector3 point;
	bg::math::Vector3 normal;
	const Material * materialPtr = nullptr;
};

class Hitable {
public:
	virtual bool hit(const Ray & ray, real_t t_min, real_t t_max, HitRecord & rec) const = 0;
	virtual bool boundingBox(float t0, float t1, AABB & box) const = 0;
};

#endif
