#ifndef _FRAME_H_
#define _FRAME_H_

#include <math_tools.hpp>

class Frame {
public:
	Frame() {
		_data = new unsigned char[_width * _height * _bpp];
		_alpha = new bool[_width * _height];
	}
	Frame(int w, int h, int bpp, int aaSamples) :_width(w), _height(h), _bpp(bpp), _antialiasSamples(aaSamples) {
		_data = new unsigned char[_width * _height * _bpp];
		_alpha = new bool[_width * _height];
	}
	virtual ~Frame() {
		delete[] _data;
		delete[] _alpha;
	}

	inline int width() const { return _width; }
	inline int height() const { return _height; }
	inline int bpp() const { return _bpp; }
	inline int antialiasSamples() const { return _antialiasSamples; }
	inline const unsigned char * data() const { return _data; }
	inline unsigned char * data() { return _data; }
	inline const bool * alpha() const { return _alpha; }
	inline bool * alpha() { return _alpha; }
	inline bg::math::Vector3uc getColor(int x, int y) const {
		auto i = stride() * x + y * _bpp;
		return bg::math::Vector3uc(_data[i], _data[i + 1], _data[i + 2]);
	}
	inline bool getAlpha(int x, int y) const { return _alpha[_width * x + y]; }

	inline int stride() const { return _width * _bpp; }

	inline void setPixel(int x, int y, unsigned char r, unsigned char g, unsigned char b) {
		int s = stride();
		_data[s * x + y * _bpp] = r;
		_data[s * x + y * _bpp + 1] = g;
		_data[s * x + y * _bpp + 2] = b;
	}
	inline void setAlpha(int x, int y, bool alpha) {
		_alpha[_width * x + y] = alpha;
	}

protected:
	int _width = 200;
	int _height = 100;
	int _bpp = 3;
	int _antialiasSamples = 10;
	unsigned char * _data;
	bool * _alpha;

};

#endif
