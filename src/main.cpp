

#include <iostream>
#include <memory>

#include <image.hpp>

#include <math_tools.hpp>
#include <ray.hpp>
#include <sphere.hpp>
#include <hitable_list.hpp>
#include <camera.hpp>
#include <frame.hpp>
#include <material.hpp>
#include <command_line.hpp>
#include <bvh_node.hpp>
#include <mesh.hpp>
#include <bg2e_model.hpp>
#include <renderer.hpp>

#include <random>
#include <thread>
#include <ctime>
#include <iomanip>
#include <mutex>
#include <atomic>
#include <memory>

using namespace bg::math;

bool renderMap(const std::string & outFile, const std::string & sceneName, const std::string & inFile, HitableList * list, Frame & frame, Renderer & renderer, int blurRadius) {
	Model * model = nullptr;
	
    if (!sceneName.empty()) {
        std::cout << "Rendering scene lightmap from " << sceneName << " to model " << inFile << std::endl;
        Hitable * scene = Bg2eScene::Load(sceneName, inFile, model);
        if (scene) {
			list->addItem(scene);
        }
        else {
            std::cout << "Could not load scene " << sceneName << ": no such file or directory." << std::endl;
			return false;
        }
    }
    else {
		try {
			model = new Model();
			model->load(inFile);
		}
		catch (std::exception & e) {
			std::cerr << e.what() << std::endl;
			return false;
		}
        std::cout << "Generating global ilumination for model " << inFile << std::endl;
        Hitable * mesh = Bg2eModel::Load(model);
		if (mesh) {
			list->addItem(mesh);
		}
		else {
			std::cerr << "Could not create mesh from model " << inFile << std::endl;
			return false;
		}
    }
    model->lightmap().saveMap(outFile, renderer, list, blurRadius, frame);
	delete model;
	return true;
}

int main(int argc, char ** argv) {
	CommandLine cl(argc, const_cast<const char**>(argv));
	std::string imagePath = cl.outFile();
	bool multithreaded = cl.multithread();
	int width = cl.width();
	int height = cl.height();
	int antialiasingSamples = cl.samples();
    
    if (!bg::math::isPowerOfTwo(width) || !bg::math::isPowerOfTwo(height)) {
        std::cerr << "Error: could not generate map. The image size is not power of two: " << width << "x" << height << std::endl;
        return 1;
    }

	Renderer renderer;
	renderer.setBuckets(32, 32);
	renderer.setMultithreaded(multithreaded);

	std::unique_ptr<Image> img = std::make_unique<Image>();
	Frame frame(width,height,3,antialiasingSamples);

	std::unique_ptr<HitableList> worldData = std::make_unique<HitableList>();
    renderMap(cl.outFile(), cl.inScene(), cl.inFile(), worldData.get(), frame, renderer, cl.blurRadius());

	return 0;
}
