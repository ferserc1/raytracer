
#include <bvh_node.hpp>

#include <cstdlib>
#include <iostream>

int boxXCompare(const void * a, const void * b) {
	AABB boxLeft, boxRight;
	Hitable * ah = *(Hitable**)a;
	Hitable * bh = *(Hitable**)b;
	if (!ah->boundingBox(0, 0, boxLeft) || !bh->boundingBox(0, 0, boxRight)) {
		std::cerr << "No bounding box in BVHNode constructor" << std::endl;
	}
	if (boxLeft.min().x() - boxRight.min().x() < 0.0) {
		return -1;
	}
	else {
		return 1;
	}
}

int boxYCompare(const void * a, const void * b) {
	AABB boxLeft, boxRight;
	Hitable * ah = *(Hitable**)a;
	Hitable * bh = *(Hitable**)b;
	if (!ah->boundingBox(0, 0, boxLeft) || !bh->boundingBox(0, 0, boxRight)) {
		std::cerr << "No bounding box in BVHNode constructor" << std::endl;
	}
	if (boxLeft.min().y() - boxRight.min().y() < 0.0) {
		return -1;
	}
	else {
		return 1;
	}
}

int boxZCompare(const void * a, const void * b) {
	AABB boxLeft, boxRight;
	Hitable * ah = *(Hitable**)a;
	Hitable * bh = *(Hitable**)b;
	if (!ah->boundingBox(0, 0, boxLeft) || !bh->boundingBox(0, 0, boxRight)) {
		std::cerr << "No bounding box in BVHNode constructor" << std::endl;
	}
	if (boxLeft.min().z() - boxRight.min().z() < 0.0) {
		return -1;
	}
	else {
		return 1;
	}
}

BVHNode::BVHNode()
{

}

BVHNode::BVHNode(Hitable ** l, size_t listLength, float time0, float time1)
{
	int axis = static_cast<int>(3.0 * bg::math::random());
	if (axis == 0) {
		qsort(l, listLength, sizeof(Hitable*), boxXCompare);
	}
	else if (axis == 1) {
		qsort(l, listLength, sizeof(Hitable*), boxYCompare);
	}
	else {
		qsort(l, listLength, sizeof(Hitable*), boxZCompare);
	}

	if (listLength == 1) {
		_left = _right = l[0];
	}
	else if (listLength == 2) {
		_left  = l[0];
		_right = l[1];
	}
	else {
		_left  = new BVHNode(l, listLength / 2, time0, time1);
		_right = new BVHNode(l + listLength / 2, listLength - listLength / 2, time0, time1);
	}

	AABB boxLeft, boxRight;
	if (!_left->boundingBox(time0, time1, boxLeft) || !_right->boundingBox(time0, time1, boxRight)) {
		std::cerr << "No bounding box in BVHNode constructor" << std::endl;
	}
	_box = AABB::SurroundingBox(boxLeft, boxRight);
}

bool BVHNode::hit(const Ray & r, real_t tmin, real_t tmax, HitRecord & rec) const {
	if (_box.hit(r, tmin, tmax)) {
		HitRecord leftRec, rightRec;
		bool hitLeft = _left->hit(r, tmin, tmax, leftRec);
		bool hitRight = _left->hit(r, tmin, tmax, rightRec);
		if (hitLeft && hitRight) {
			if (leftRec.distance < rightRec.distance) {
				rec = leftRec;
			}
			else {
				rec = rightRec;
			}
			return true;
		}
		else if (hitLeft) {
			rec = leftRec;
			return true;
		}
		else if (hitRight) {
			rec = rightRec;
			return true;
		}
	}
	return false;
}

bool BVHNode::boundingBox(float t0, float t1, AABB & box) const {
	box = _box;
	return true;
}