
#include <mesh.hpp>

#include <iostream>

using namespace bg::math;

Mesh::Mesh(const std::vector<real_t> & vert, const std::vector<real_t> & norm, const std::vector<uint32_t> & index, Material * mat)
	:_material(mat)
{
	setVertexArray(vert);
	setNormalArray(norm);
	setIndexArray(index);
}

void Mesh::setVertexArray(const std::vector<real_t> & vert) {
	if (vert.size() % 3 != 0) {
		throw std::runtime_error("Invalid vector set: the vertex array must be multiple of three");
	}

	Vector3 min(bg::math::number::maxValue(0.0));
	Vector3 max(bg::math::number::maxValue(0.0));
	for (auto i = 0; i < vert.size(); i+=3) {
		Vector3 v(vert[i], vert[i + 1], vert[i + 2]);
		_v.push_back(v);

		if (v[0] < min[0]) { min[0] = v[0]; }
		if (v[1] < min[1]) { min[1] = v[1]; }
		if (v[2] < min[2]) { min[2] = v[2]; }
		if (v[0] > max[0]) { max[0] = v[0]; }
		if (v[1] > max[1]) { max[1] = v[1]; }
		if (v[2] > max[2]) { max[2] = v[2]; }
	}

	_box = AABB(min, max);
}

void Mesh::setNormalArray(const std::vector<real_t> & norm) {
	if (norm.size() % 3 != 0) {
		throw std::runtime_error("Invalid vector set: the normal array must be multiple of three");
	}
	for (auto i = 0; i < norm.size(); i += 3) {
		Vector3 n(norm[i], norm[i + 1], norm[i + 2]);
		_n.push_back(n);
	}
}

void Mesh::setIndexArray(const std::vector<uint32_t> & index) {
	if (index.size() % 3 != 0) {
		std::cerr << "Warning: invalid vector set: the index array must be multiple of three. Some indexes will be ignored." << std::endl;
	}
	_index = index;
}

bool Mesh::hit(const Ray & ray, real_t tmin, real_t tmax, HitRecord & rec) const {
	bool hitSomething = false;
	real_t closestTriangle = tmax;
	HitRecord tmpRec;
	if (!_box.hit(ray, tmin, tmax)) {
		return false;
	}

	for (auto i = 0; i < _index.size(); i+=3) {
		if (hitTriangle(_index[i + 0], _index[i + 1], _index[i + 2], ray, tmin, closestTriangle, tmpRec)) {
			hitSomething = true;
			rec = tmpRec;
			rec.materialPtr = _material;
			closestTriangle = tmpRec.distance;
		}
	}
	return hitSomething;
}

bool Mesh::hitTriangle(uint32_t v0i, uint32_t v1i, uint32_t v2i, const Ray & ray, real_t tmin, real_t tmax, HitRecord & rec) const {
	const Vector3 & v0 = _v[v0i]; const Vector3 & n0 = _n[v0i];
	const Vector3 & v1 = _v[v1i]; const Vector3 & n1 = _n[v1i];
	const Vector3 & v2 = _v[v2i]; const Vector3 & n2 = _n[v2i];

	Vector3 v0v1 = v1 - v0;
	Vector3 v0v2 = v2 - v0;

	Vector3 pvec = cross(ray.direction(), v0v2);
	real_t det = dot(v0v1, pvec);

	bool singleSided = true;
	if (singleSided && det < bg::math::epsilon(0.0)) {
		return false;	// back face
	}
	
	if (bg::math::abs(det) < bg::math::epsilon(0.0)) {
		return false;	// parallel
	}

	// Calculate barycentric coords.
	real_t invDet = 1.0 / det;
	Vector3 tvec = ray.origin() - v0;
	
	real_t u = dot(tvec, pvec) * invDet;
	if (u<0.0 || u>1.0) return false;

	Vector3 qvec = cross(tvec, v0v1);
	real_t v = dot(ray.direction(), qvec) * invDet;
	if (v<0.0 || u + v>1.0) return false;

	// Result
	rec.distance = dot(v0v2, qvec) * invDet;
	rec.point = ray.origin() + ray.direction() * rec.distance;
	rec.normal = n0;	// TODO: use u,v to interpolate normal

	return rec.distance <= tmax && rec.distance >= 0;
}

