
#include <material.hpp>

using namespace bg::math;

bg::math::Vector3 randomInUnitSphere() {
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<> dis(0.0f, 1.0f);
	Vector3 p;
	do {
		real_t rx = static_cast<float>(dis(gen)),
			ry = static_cast<float>(dis(gen)),
			rz = static_cast<float>(dis(gen));
		p = 2.0 * Vector3(rx, ry, rz) - Vector3(1.0, 1.0, 1.0);
	} while (p.squaredMagnitude() >= 1.0);
	return unitVector(p);
}

bg::math::Vector3 reflect(const bg::math::Vector3 & vector, const bg::math::Vector3 & normal) {
	return vector - 2.0 * dot(vector, normal) * normal;
}

bool refract(const bg::math::Vector3 & vec, const bg::math::Vector3 & normal, real_t ni_over_nt, bg::math::Vector3 & refracted) {
	using namespace bg::math;
	Vector3 uv = unitVector(vec);
	real_t dt = dot(uv, normal);
	real_t discriminant = 1.0 - ni_over_nt * ni_over_nt * (1.0 - dt * dt);
	if (discriminant > 0.0) {
		refracted = ni_over_nt * (uv - normal * dt) - normal * bg::math::sqrt(discriminant);
		return true;
	}
	else {
		return false;
	}
}

real_t schlick(real_t cosine, real_t refractionIndex) {
	real_t r0 = (1.0 - refractionIndex) / (1.0 + refractionIndex);
	r0 = r0 * r0;
	return r0 + (1.0 - r0) * bg::math::pow(1.0 - cosine, 5);
}

bool Lambertian::scatter(const Ray & ray, const HitRecord & rec, bg::math::Vector3 & attenuation, Ray & scattered) const {
	bg::math::Vector3 target = rec.point + rec.normal + randomInUnitSphere();
	bg::math::Vector3 targetRay = unitVector(target - rec.point);
	scattered = Ray(rec.point, targetRay, ray.time());
	attenuation = albedo();
	return true;
}
bool Metal::scatter(const Ray & ray, const HitRecord & rec, bg::math::Vector3 & attenuation, Ray & scattered) const {
	scattered = Ray(rec.point, reflect(unitVector(ray.direction()), rec.normal) + _fuzz * randomInUnitSphere(), ray.time());
	attenuation = albedo();
	return (dot(scattered.direction(), rec.normal) > 0);
}

bool Dielectric::scatter(const Ray & ray, const HitRecord & rec, bg::math::Vector3 & attenuation, Ray & scattered) const {
	Vector3 outwardNormal;
	Vector3 reflected = reflect(ray.direction(), rec.normal);
	real_t ni_over_nt;
	attenuation = Vector3(1.0, 1.0, 1.0);
	Vector3 refracted;
	real_t reflectProb;
	real_t cosine;
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<> dis(0.0f, 1.0f);

	if (dot(ray.direction(), rec.normal) > 0.0) {
		outwardNormal = -1.0 * rec.normal;
		ni_over_nt = _refractionIndex;
		cosine = _refractionIndex * dot(ray.direction(), rec.normal) / ray.direction().magnitude();
	}
	else {
		outwardNormal = rec.normal;
		ni_over_nt = 1.0 / _refractionIndex;
		cosine = -dot(ray.direction(), rec.normal) / ray.direction().magnitude();
	}

	if (refract(ray.direction(), outwardNormal, ni_over_nt, refracted)) {
		reflectProb = schlick(cosine, _refractionIndex);
	}
	else {
		scattered = Ray(rec.point, reflected, ray.time());
		reflectProb = 1.0;
	}

	if (dis(gen) < reflectProb) {
		scattered = Ray(rec.point, reflected, ray.time());
	}
	else {
		scattered = Ray(rec.point, refracted, ray.time());
	}
	return true;
}
