
#include <aabb.hpp>

using namespace bg::math;

AABB AABB::SurroundingBox(const AABB & box0, const AABB & box1) {
	Vector3 small(ffmin(box0.min().x(), box1.min().x()),
				  ffmin(box0.min().y(), box1.min().y()),
				  ffmin(box0.min().z(), box1.min().z()));
	Vector3 big(  ffmax(box0.max().x(), box1.max().x()),
				  ffmax(box0.max().y(), box1.max().y()),
				  ffmax(box0.max().z(), box1.max().z()));
	return AABB(small, big);
}

AABB::AABB() {}

AABB::AABB(const bg::math::Vector3 & a, const bg::math::Vector3 & b)
	:_min(a)
	,_max(b)
{

}

bool AABB::hit(const Ray & r, real_t tmin, real_t tmax) const {
	// Method 1
	/*
	for (int a = 0; a < 3; ++a) {
		real_t t0 = ffmin((_min[a] - r.origin()[a]) / r.direction()[a],
						  (_max[a] - r.origin()[a]) / r.direction()[a]);
		real_t t1 = ffmax((_min[a] - r.origin()[a]) / r.direction()[a],
						  (_max[a] - r.origin()[a]) / r.direction()[a]);
		tmin = ffmax(t0, tmin);
		tmax = ffmin(t1, tmax);
		if (tmax <= tmin) {
			return false;
		}
	}
	return true;
	*/

	// Method 2
	for (int a = 0; a < 3; ++a) {
		real_t invD = 1.0 / r.direction()[a];
		real_t t0 = (min()[a] - r.origin()[a]) * invD;
		real_t t1 = (max()[a] - r.origin()[a]) * invD;
		if (invD < 0.0) {
			std::swap(t0, t1);
		}
		tmin = t0 > tmin ? t0 : tmin;
		tmax = t1 < tmax ? t1 : tmax;
		if (tmax <= tmin) {
			return false;
		}
	}
	return true;
	
}
