
#include <bg2e_model.hpp>
#include <hitable_list.hpp>
#include <material.hpp>
#include <bvh_node.hpp>

#include <bg2model/json/parser.hpp>

#include <model.hpp>

#include <iostream>
#include <regex>

#include <bg2model/bg2_reader.hpp>

Hitable * Bg2eModel::Load(const std::string & path, const bg::math::Matrix4 & transform) {
	Model model;

	try {
		model.load(path, transform);
	}
	catch (std::exception & e) {
		std::cerr << e.what() << std::endl;
		return nullptr;
	}
	return Load(&model);
}

Hitable * Bg2eModel::Load(const Model * model) {
	HitableList * meshes = new HitableList();
	for (const auto & plist : model->polyLists()) {
		meshes->addItem(new Mesh(plist->vertex, plist->normal, plist->index, new Lambertian(Vector3(0.9, 0.9, 0.9))));
	}

	return meshes;
}


class MatrixStack {
public:
    inline void push() {
        _stack.push_back(_matrix);
    }
    
    inline void pop() {
        _matrix = _stack.back();
        _stack.pop_back();
    }
    
    inline const bg::math::Matrix4 & matrix() const { return _matrix; }
	inline bg::math::Matrix4 & matrix() { return _matrix; }
    
protected:
    bg::math::Matrix4 _matrix = bg::math::Matrix4::Identity();
    
    std::vector<bg::math::Matrix4> _stack;
};

void parseDrawable(const std::string & name, MatrixStack & matrixStack, Hitable * world, const std::string & scenePath, const std::string & drawableName, Model *& modelResult) {
	Hitable * model = nullptr;
    std::string filePath = scenePath + name;
	if (drawableName == name && !modelResult) {
		try {
			modelResult = new Model();
			if (!modelResult->load(filePath + ".vwglb", matrixStack.matrix())) {
				modelResult->load(filePath + ".bg2", matrixStack.matrix());
			}
			model = Bg2eModel::Load(modelResult);
		}
		catch (std::runtime_error & err) {
			std::cerr << err.what() << std::endl;
			return;
		}
	}
	else {
		model = Bg2eModel::Load(filePath + ".vwglb", matrixStack.matrix());
		model = !model ? Bg2eModel::Load(filePath + ".bg2", matrixStack.matrix()) : model;
	}
	HitableList * list = dynamic_cast<HitableList*>(world);
	if (list && model) {
		list->addItem(model);
	}
	else if (list) {
		std::cerr << "Warning: model no such model " + filePath + ".vwglb or " + filePath + ".bg2" << std::endl;
	}
	else {
		std::cerr << "Unexpected error: world node is not a hitable list" << std::endl;
	}
}

void parseNode(bg2model::json::Value * node, Hitable * world, MatrixStack & matrixStack, const std::string & scenePath, const std::string & drawableName, Model *& model) {
	using namespace bg2model;
    if (!node || node->type()!=json::Value::kObject) {
        throw std::runtime_error("Unexpected error: invalid scene node");
    }
	json::Value * components = (*node)["components"];
    bool steady = json::Value::Bool((*node)["steady"],false);
	matrixStack.push();
	if (components) {
		std::vector<real_t> matrixData;
		std::string drawable = "";
		components->eachItem([&](json::Value * component) {
			if (json::Value::String((*component)["type"]) == "Transform" &&
				(*component)["transformMatrix"] &&
				(*component)["transformMatrix"]->type()==json::Value::kArray
			) {
				(*component)["transformMatrix"]->eachItem([&](json::Value * item) {
					if (item->type() == json::Value::kNumber) {
						matrixData.push_back(item->floatValue());
					}
				});
			}
			else if (json::Value::String((*component)["type"]) == "Drawable") {
				drawable = json::Value::String((*component)["name"]);
			}
		});
		if (matrixData.size() == 16) {
			if (matrixData.size() == 16) {
				matrixStack.matrix().mult(bg::math::Matrix4(matrixData.data()));
			}
		}
		if (!drawable.empty() && steady) {
			parseDrawable(drawable, matrixStack, world, scenePath, drawableName, model);
		}
	}
	json::Value * children = (*node)["children"];
	if (children) {
		children->eachItem([&](json::Value * child) {
			parseNode(child, world, matrixStack, scenePath, drawableName, model);
		});
	}
	matrixStack.pop();
}

Hitable * parseScene(bg2model::json::Value * sceneData, const std::string & drawableName, const std::string & scenePath, Model *& model) {
    using namespace bg2model;
    Hitable * world = new HitableList();
    MatrixStack matrixStack;
    if (!sceneData && sceneData->type() != json::Value::kArray) {
        throw std::runtime_error("Invalid scene root node");
    }
    
    sceneData->eachItem([&](json::Value * item, size_t index, const json::Value::ValueArray & array) {
        parseNode(item, world, matrixStack, scenePath, drawableName, model);
    });
    
    return world;
}

Hitable * Bg2eScene::Load(const std::string & path, const std::string & drawableName, Model *& model) {
	model = nullptr;
    using namespace bg2model;
    std::string standarizedPath = path;
    std::regex replace("\\\\");
    std::regex_replace(standarizedPath.begin(), path.begin(), path.end(), replace, "/");
    auto pos = standarizedPath.find_last_of('/');
    std::string scenePath = standarizedPath;
    if (pos!=std::string::npos) {
        scenePath.replace(scenePath.begin() + pos + 1,scenePath.end(),"");
    }
    else {
        scenePath = "";
    }
    try {
        json::Value * jsonDoc = json::Parser::ParseFile(standarizedPath);
        return parseScene((*jsonDoc)["scene"], drawableName, scenePath, model);
    }
    catch (std::exception & e) {
        std::cerr << e.what() << ". File: " << path << std::endl;
        return nullptr;
    }
    
    
    return nullptr;
}
