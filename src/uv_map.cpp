
#include <uv_map.hpp>

#include <image.hpp>

#include <material.hpp>

#include <iostream>
#include <iomanip>

inline float * boxesForGauss(float sigma, float n) {
    float wIdeal = bg::math::sqrt((12.0f * sigma * sigma / n) + 1.0f);
    float wl = static_cast<float>(floorf(wIdeal));
    if (static_cast<int>(wl)%2==0) {
        wl--;
    }
    float wu = wl + 2.0f;
    float mIdeal = (12.0f * sigma * sigma - n*wl*wl - 4.0f*n*wl - 3.0f*n)/(-4.0f*wl - 4.0f);
    
    float m = roundf(mIdeal);
    // var sigmaActual = Math.sqrt( (m*wl*wl + (n-m)*wu*wu - n)/12 );
    
    float * result = new float[n];
    for(auto i=0; i<n; ++i) {
        result[i] = i<m ? wl : wu;
    }
    return result;
}

inline void boxBlur_2 (const unsigned char * scl, unsigned char * tcl, int w, int h, int r) {
    for(auto i=0; i<h; ++i)
        for(auto j=0; j<w; ++j) {
            float valR = 0.0f;
            float valG = 0.0f;
            float valB = 0.0f;
            for(auto iy=i-r; iy<i+r+1; ++iy)
                for(auto ix=j-r; ix<j+r+1; ++ix) {
                    int x = bg::math::min(w-1, bg::math::max(0, ix));
                    int y = bg::math::min(h-1, bg::math::max(0, iy));
                    valR += static_cast<float>(scl[y * w * 3 + x * 3]);
                    valG += static_cast<float>(scl[y * w * 3 + x * 3 + 1]);
                    valB += static_cast<float>(scl[y * w * 3 + x * 3 + 2]);
                }
            valR = static_cast<unsigned char>(valR / static_cast<float>((r+r+1) * (r+r+1)));
            valG = static_cast<unsigned char>(valG / static_cast<float>((r+r+1) * (r+r+1)));
            valB = static_cast<unsigned char>(valB / static_cast<float>((r+r+1) * (r+r+1)));
            tcl[i * w * 3 + j * 3] = valR;
            tcl[i * w * 3 + j * 3 + 1] = valG;
            tcl[i * w * 3 + j * 3 + 2] = valB;
        }
}

inline void gaussBlur(unsigned char * scl, unsigned char * tcl, int w, int h, int r) {
    float * bxs = boxesForGauss(static_cast<float>(r), 3.0f);
    boxBlur_2 (scl, tcl, w, h, (bxs[0]-1)/2);
    boxBlur_2 (tcl, scl, w, h, (bxs[1]-1)/2);
    boxBlur_2 (scl, tcl, w, h, (bxs[2]-1)/2);
    delete[] bxs;
}

inline bool toBarycentric(const bg::math::Vector2i & a, const bg::math::Vector2i & b, const bg::math::Vector2i & c, int x, int y, bg::math::Vector3 & result) {
	real_t X1 = static_cast<real_t>(a.x());
	real_t X2 = static_cast<real_t>(b.x());
	real_t X3 = static_cast<real_t>(c.x());
	real_t X4 = static_cast<real_t>(x);
	real_t Y1 = static_cast<real_t>(a.y());
	real_t Y2 = static_cast<real_t>(b.y());
	real_t Y3 = static_cast<real_t>(c.y());
	real_t Y4 = static_cast<real_t>(y);

	real_t detM = X1 * Y2 - X1 * Y3 - X2 * Y1 + X2 * Y3 + X3 * Y1 - X3 * Y2;
	if (bg::math::abs(detM) == bg::math::epsilon(0.0)) {
		return false;
	}

	real_t detM1 = X4 * Y2 - X4 * Y3 - X2 * Y4 + X2 * Y3 + X3 * Y4 - X3 * Y2;
	real_t detM2 = X1 * Y4 - X1 * Y3 - X4 * Y1 + X4 * Y3 + X3 * Y1 - X3 * Y4;
	real_t detM3 = X1 * Y2 - X1 * Y4 - X2 * Y1 + X2 * Y4 + X4 * Y1 - X4 * Y2;

	result.set(detM1 / detM, detM2 / detM, detM3 / detM);
	return true;
}

inline bg::math::Vector2i toPixel(const bg::math::Vector2 & v, uint32_t w, uint32_t h) {
	return bg::math::Vector2i(
		static_cast<uint32_t>(bg::math::round(v.x() * static_cast<real_t>(w))),
		static_cast<uint32_t>(bg::math::round(v.y() * static_cast<real_t>(h)))
	);
}

inline bool hitTriangle(const bg::math::Vector2i v0, const bg::math::Vector2i & v1, const bg::math::Vector2i & v2, uint32_t x, uint32_t y) {
	bg::math::Vector3 barycentric;
	if (toBarycentric(v0, v1, v2, x, y, barycentric)) {
		return barycentric.x()>=0.0 && barycentric.x() <= 1.0 &&
			barycentric.y() >= 0.0 && barycentric.y() <= 1.0 &&
			barycentric.z() >= 0.0 && barycentric.z() <= 1.0;
	}
	else {
		return false;
	}
}

inline bool insideTriangle(const bg::math::Vector3 & barycentric) {
	return	barycentric.x() > 0.0 && barycentric.x() < 1.0 &&
			barycentric.y() > 0.0 && barycentric.y() < 1.0 &&
			barycentric.z() > 0.0 && barycentric.z() < 1.0;
}

void UVMap::clear() {
	_triangles.clear();
}

using namespace bg::math;

Vector3 get_color(const Ray & r, Hitable * world, int indirectRayIndex = 0) {
	static const int s_maxIndirectRays = 10;
	HitRecord rec;
	if (world->hit(r, 0.001, bg::math::number::maxValue(0.0), rec)) {
		Ray scattered;
		Vector3 attenuation;
		if (indirectRayIndex < s_maxIndirectRays && rec.materialPtr->scatter(r, rec, attenuation, scattered)) {
			return attenuation *get_color(scattered, world, indirectRayIndex + 1);
		}
		else {
			return Vector3(0.0, 0.0, 0.0);
		}
	}
	else {
		Vector3 unitDirection = unitVector(r.direction());
		real_t t = 0.5 * (unitDirection.y() + 1.0);
		return (1.0 - t) * Vector3(1.0) + t * Vector3(0.8, 0.8, 0.8);
	}
}

Ray getRay(const Vector3 & point, float u, float v) {
	return Ray();
}


void UVMap::renderBucket(Frame & frame, Hitable * world, int left, int bottom, int width, int height) const {
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<> dis(0.0f, 1.0f);
	for (int j = bottom; j < height + bottom && j<frame.height(); ++j) {
		for (int i = left; i < width + left && i < frame.width(); ++i) {
			Vector3 col(0.0, 0.0, 0.0);
			Vector3 point, normal;
			int x = frame.height() - 1 - j;
			int y = i;

			if (getPoint(frame, i, j, point, normal)) {
				for (int s = 0; s < frame.antialiasSamples(); s++) {
					Vector3 origin = point + normal * 0.1;
					Ray r(origin, -1.0 * normal, 0);
					col = col + get_color(r, world);
				}
				col /= static_cast<float>(frame.antialiasSamples());
				// Gamma correction
				col = Vector3(bg::math::sqrt(col.r()), bg::math::sqrt(col.g()), bg::math::sqrt(col.b()));
				frame.setAlpha(x, y, true);
			}
			else {
				frame.setAlpha(x, y, false);
			}
			
			unsigned char ir = static_cast<unsigned char>(255.0f * col[0]);
			unsigned char ig = static_cast<unsigned char>(255.0f * col[1]);
			unsigned char ib = static_cast<unsigned char>(255.0f * col[2]);

			frame.setPixel(x, y, ir, ig, ib);
		}
	}
}

bool UVMap::getPoint(const Frame & frame, int x, int y, bg::math::Vector3 & point, bg::math::Vector3 & normal) const {
	for (auto & t : _triangles) {
		Vector2i v0i = toPixel(t.t0, frame.width(), frame.height());
		Vector2i v1i = toPixel(t.t1, frame.width(), frame.height());
		Vector2i v2i = toPixel(t.t2, frame.width(), frame.height());

		bg::math::Vector3 barycentric;
		if (toBarycentric(v0i, v1i, v2i, x, y, barycentric) && insideTriangle(barycentric)) {
			point.set(
				t.v0.x() * barycentric.x() + t.v1.x() * barycentric.y() + t.v2.x() * barycentric.z(),
				t.v0.y() * barycentric.x() + t.v1.y() * barycentric.y() + t.v2.y() * barycentric.z(),
				t.v0.z() * barycentric.x() + t.v1.z() * barycentric.y() + t.v2.z() * barycentric.z()
			);
			Vector3 v01 = t.v0 - t.v1;
			Vector3 v02 = t.v0 - t.v2;
			normal = cross(v01, v02);
			normal.normalize();
			return true;
		}
	}
	return false;
}

bg::math::Vector3uc closestPixel(const Frame & frame, int x, int y) {
	bool found = false;
	int resX = 0;
	int resY = 0;
	int maxDistance = 20;
	float minDistance = bg::math::sqrt(static_cast<float>(maxDistance*maxDistance*2));
	
	for (int distance = 0; !found && distance < maxDistance; ++distance) {
		for (int i = -distance; i <= distance && x + i >= 0 && x + i< frame.width(); ++i) {
			for (int j = -distance; j <= distance && y + j >= 0 && y + j < frame.height(); ++j) {
				if (frame.getAlpha(x + i, y + j)) {
					float d = bg::math::sqrt(static_cast<float>(i * i + j * j));
					if (minDistance > d) {
						minDistance = d;
						resX = x + i;
						resY = y + j;
						found = true;
					}
				}
			}
		}
	}

	return found ? frame.getColor(resX,resY) : Vector3uc(128,128,128);
}

void UVMap::saveMap(const std::string & path, Renderer & renderer, Hitable * world, int blurRadius, Frame & frame) const {
	using namespace bg::math;

    auto begin = std::chrono::system_clock::now();
    
    std::cout << std::endl << "Rendering lightmap..." << std::endl;
	auto elapsed = renderer.render(frame.width(), frame.height(), [&](int left, int bottom, int width, int height) {
		renderBucket(frame, world, left, bottom, width, height);
	});
    
    std::cout << "Done in " << elapsed << " seconds." << std::endl;
    std::cout << std::endl << "Expanding bounding pixels..." << std::endl;

	elapsed = renderer.render(frame.width(), frame.height(), [&](int left, int bottom, int width, int height) {
		for (int j = bottom; j < height + bottom && j<frame.height(); ++j) {
			for (int i = left; i < width + left && i < frame.width(); ++i) {
				int x = frame.height() - 1 - j;
				int y = i;
				if (!frame.getAlpha(x, y)) {
					Vector3uc closest = closestPixel(frame, x, y);
					frame.setPixel(x, y, closest.x(), closest.y(), closest.z());
				}
			}
		}
	});
    std::cout << "Done in " << elapsed << " seconds." << std::endl << std::endl;

    Image img;
    unsigned char * blurData = nullptr;
    if (blurRadius>0) {
        std::cout << "Applying blur..." << std::endl;
        blurData = new unsigned char[frame.width() * frame.height() * 3];
        gaussBlur(frame.data(), blurData, frame.width(), frame.height(), blurRadius);
        img.setData(blurData, frame.width(), frame.height(), frame.bpp());
    }
    else {
        img.setData(frame.data(), frame.width(), frame.height(), frame.bpp());
    }
    
	img.save(path);
    if (blurData) {
        delete[] blurData;
    }
    
    auto end = std::chrono::system_clock::now();
    //elapsed = static_cast<float>(end - begin) / CLOCKS_PER_SEC;
    std::chrono::duration<float> totalElapsed = end - begin;
    std::cout << "All jobs done. Total time: " << std::fixed << std::setprecision(2) << totalElapsed.count() << " seconds." << std::endl;
}

