
#include <image.hpp>

#include <stb_image.h>
#include <stb_image_write.h>

#include <iostream>
#include <regex>

Image * Image::Load(const std::string & path) {
	Image * img = new Image();
	try {
		img->loadFromFile(path);
	}
	catch (std::exception & er) {
		delete img;
		std::cerr << er.what() << std::endl;
	}
	return img;
}

void Image::save(const std::string & path) {
	if (!valid()) {
		throw std::runtime_error("Could not save image: the image is not valid");
	}

	std::regex rx(".*\\.([a-z]+)$");
	std::smatch match;
	std::string ext;
	if (std::regex_match(path, match, rx)) {
		if (match.size() > 1) {
			ext = match[1].str();
		}

		int stat = 0;
		if (ext == "jpg") {
			stat = stbi_write_jpg(path.c_str(), _width, _height, _channels, _data, 100);
		}
		else if (ext == "bmp") {
			stat = stbi_write_bmp(path.c_str(), _width, _height, _channels, _data);
		}
		else if (ext == "png") {
			stat = stbi_write_png(path.c_str(), _width, _height, _channels, _data, _width * _channels);
		}
		else if (ext == "tga") {
			stat = stbi_write_tga(path.c_str(), _width, _height, _channels, _data);
		}
		else {
			throw std::runtime_error("Unsupported output file format: " + ext);
		}

		if (stat == 0) {
			throw std::runtime_error("Error saving image at path: " + path);
		}
	}
	else {
		throw std::runtime_error("Invalid output file path");
	}
}

void Image::loadFromFile(const std::string & path) {
	release();
	int w, h, bpp;
	unsigned char * data = stbi_load(path.c_str(), &w, &h, &bpp, 0);
	if (!data) {
		throw std::runtime_error("Error loading image at " + path);
	}
	else {
		setData(data, w, h, bpp);
	}
}

void Image::release() {
	if (_data) {
		delete[] _data;
		_data = nullptr;
		_width = 0;
		_height = 0;
		_channels = 0;
	}
}

void Image::setData(unsigned char * data, int w, int h, int channels) {
	release();
	_data = data;
	_width = w;
	_height = h;
	_channels = channels;
}