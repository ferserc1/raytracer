
#include <renderer.hpp>

#include <math_tools.hpp>


#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <ctime>
#include <atomic>

Renderer::Renderer() {

}

Renderer::Renderer(bool mt)
	:_multithreaded(mt)
{

}

Renderer::Renderer(bool mt, int bh, int bv)
	:_multithreaded(mt)
	,_bucketsHorizontal(bh)
	,_bucketsVertical(bv)
{

}

float Renderer::render(int width, int height, RenderFunction fn) {
	int threads = 1;
	int totalBuckets = 1;
	int bucketsHorizontal = _bucketsHorizontal;
	int bucketsVertical = _bucketsVertical;
	if (_multithreaded) {
		threads = std::thread::hardware_concurrency();
		totalBuckets = _bucketsHorizontal * _bucketsVertical;
	}
	else {
		bucketsHorizontal = 1;
		bucketsVertical = 1;
	}

	int bucketWidth = width / bucketsHorizontal;
	int bucketHeight = height / bucketsVertical;

	std::vector<std::thread> workers;
    auto begin = std::chrono::system_clock::now();
	int bucketLeft = 0;
	int bucketBottom = 0;
	int bucketsCompleted = 0;
	int currentBucket = 0;
	std::atomic<int> threadsInUse(0);
	std::atomic<bool> done(false);
	std::mutex completionMutex;
	int lastPercent = -1;
	while (!done) {
		if (threadsInUse >= threads) {
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
		}
		else {
			threadsInUse++;
			int lft, btm;
			{
				std::lock_guard<std::mutex> l(completionMutex);
				lft = bucketLeft;
				btm = bucketBottom;
			}
			workers.push_back(std::thread([&](int left, int bottom, int w, int h, int currBucket) {
				{
					std::lock_guard<std::mutex> l(completionMutex);
					if (totalBuckets == currBucket) {
						// The last bucket is on queue
						done = true;
					}
				}
				fn(left, bottom, w, h);
				{
					std::lock_guard<std::mutex> l(completionMutex);
					bucketsCompleted++;
					threadsInUse--;
					int completedPercent = static_cast<int>(static_cast<float>(bucketsCompleted) / static_cast<float>(totalBuckets) * 100.0f);
					if (completedPercent != lastPercent) {
						// TODO: progress callback
						std::cout << completedPercent << "% completed" << std::endl;
						lastPercent = completedPercent;
					}
				}
			}, lft, btm, bucketWidth, bucketHeight, currentBucket++));
			{
				std::lock_guard<std::mutex> l(completionMutex);
				bucketLeft += bucketWidth;
				if (bucketLeft >= width) {
					bucketLeft = 0;
					bucketBottom += bucketHeight;
				}
			}
		}
	}

	for (auto & worker : workers) {
		worker.join();
	}

    auto end = std::chrono::system_clock::now();
    return std::chrono::duration<float>(end - begin).count();
}
