
#include <command_line.hpp>

#include <iostream>
#include <thread>

enum Parameter {
	kNone = 0,
	kWidth = 1,
	kHeight,
	kSamples,
	kMultithread,
    kBlur,
    kOutFile,
    kInScene
};

void printHelp() {
	std::cout << "No arguments suplied." << std::endl << std::endl;
	std::cout << "Usage: raytracer scene_or_model_file --out [output file name] --scene [input scene] --width [image width] --height [image height]"
		" --samples [samples per pixel] --multicpu [true|false] --blur [value]" << std::endl << std::endl;
	std::cout << "  Supported image formats: jpg, png, bmp, tif" << std::endl;
    std::cout << "  Blur: 0 to disable" << std::endl;
	std::cout << "  Samples per pixel: specify 1 to disable antialiasing" << std::endl << std::endl;
}
CommandLine::CommandLine(int argc, const char ** argv) {
    if (argc==1) {
		printHelp();
        std::cout << "Generating test image" << std::endl << std::endl;
    }

#ifdef _DEBUG
	_multithread = false;
#endif

	Parameter param = kNone;
	try {
		for (auto i = 1; i<argc; ++i) {
			std::string s_param(argv[i]);
			switch (param) {
			case kWidth:
				_width = std::stoi(s_param);
				param = kNone;
				break;
			case kHeight:
				_height = std::stoi(s_param);
				param = kNone;
				break;
			case kSamples:
				_samples = std::stoi(s_param);
				param = kNone;
				break;
			case kMultithread:
				_multithread = s_param == "true" || s_param == "1" ? true : false;
				param = kNone;
				break;
            case kBlur:
                _blurRadius = std::stoi(s_param);
                param = kNone;
                break;
            case kOutFile:
                _outFile = s_param;
                param = kNone;
                break;
            case kInScene:
                _inScene = s_param;
                param = kNone;
                break;
			default:
				if (s_param == "--width") {
					param = kWidth;
				}
				else if (s_param == "--height") {
					param = kHeight;
				}
				else if (s_param == "--samples") {
					param = kSamples;
				}
				else if (s_param == "--multicpu") {
					param = kMultithread;
				}
                else if (s_param == "--blur") {
                    param = kBlur;
                }
                else if (s_param == "--out") {
                    param = kOutFile;
                }
                else if (s_param == "--scene") {
                    param = kInScene;
                }
				else {
					param = kNone;
					_inFile = s_param;
				}
			}
		}
	}
	catch (std::exception &) {
		std::cerr << "Invalid command" << std::endl;
		printHelp();
	}

    std::cout << std::endl << "Input scene: " << inFile();
    std::cout << std::endl << "Output lightmap file: " << outFile() << std::endl;
    std::cout << "Image width: " << width() << " pixels." << std::endl;
	std::cout << "Image height: " << height() << " pixels." << std::endl;
	if (samples() > 1) {
		std::cout << "Antialiasing enabled with " << samples() << " samples per pixel." << std::endl;
	}
	else {
		std::cout << "Antialiasing disabled." << std::endl;
	}
    if (blurRadius()) {
        std::cout << "Blur radius: " << blurRadius() << std::endl;
    }
    if (multithread()) {
        std::cout << "Render using " << std::thread::hardware_concurrency() << " processors." << std::endl;
    }
    else {
        std::cout << "Render in single thread mode" << std::endl;
    }
}
