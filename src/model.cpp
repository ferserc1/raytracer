
#include <model.hpp>

#include <bg2model/bg2_reader.hpp>


bool Model::load(const std::string & path, const bg::math::Matrix4 & transform) {
	clear();
	bg::math::Matrix4 rotation = transform.rotation();

	bg2model::Bg2Reader reader;

	PolyList * current = nullptr;
    
    unsigned int lightmapIndexBase = 0;
    unsigned int numVertex = 0;

	std::vector<bg::math::Vector2> uv1;

	reader.version([&](uint8_t, uint8_t, uint8_t) {});

	reader.plistName([&](const std::string & name) {
		if (current) {
			_polyLists.push_back(current);
		}
		current = new PolyList();
		current->name = name;
	});

	reader.vertex([&](const std::vector<float> & vec) {
		for (auto i = 0; i < vec.size(); i += 3) {
			bg::math::Vector3 vertex(
				static_cast<real_t>(vec[i]),
				static_cast<real_t>(vec[i + 1]),
				static_cast<real_t>(vec[i + 2])
			);
			vertex = transform.multVector(vertex).xyz();
			current->vertex.push_back(vertex.x());
			current->vertex.push_back(vertex.y());
			current->vertex.push_back(vertex.z());
			_lightmap.addVertex({ vertex.x(), vertex.y(), vertex.z() });
		}
        numVertex = static_cast<unsigned int>(vec.size()) / 3;
	});

	reader.normal([&](const std::vector<float> & n) {
		for (auto i = 0; i < n.size(); i += 3) {
			bg::math::Vector3 norm(
				static_cast<real_t>(n[i]),
				static_cast<real_t>(n[i + 1]),
				static_cast<real_t>(n[i + 2])
			);
			norm = rotation.multVector(norm).xyz();
			norm.normalize();
			current->normal.push_back(norm.x());
			current->normal.push_back(norm.y());
			current->normal.push_back(norm.z());
		}
	});

	reader.uv0([&](const std::vector<float> & uv0) {
	});

	reader.uv1([&](const std::vector<float> & uv1) {
		for (auto i = 0; i < uv1.size(); i += 2) {
			_lightmap.addUV({ uv1[i], uv1[i + 1] });
		}
	});
    
	reader.index([&](const std::vector<unsigned int> & index) {
		for (auto i = 0; i < index.size(); i += 3) {
			current->index.push_back(index[i]);
			current->index.push_back(index[i + 1]);
			current->index.push_back(index[i + 2]);
			_lightmap.addTriangle(index[i] + lightmapIndexBase, index[i + 1] + lightmapIndexBase, index[i + 2] + lightmapIndexBase);
		}
        lightmapIndexBase += numVertex;
	});

	std::exception err;
	reader.error([&](const std::exception & e) {
		err = e;
	});
	reader.load(path);

	if (current) {
		// Save the last element
		_polyLists.push_back(current);
		return true;
	}
	else if (err.what()==nullptr) {
		return false;
	}
	else {
		throw err;
	}
}

void Model::clear() {
	for (auto pl : _polyLists) {
		delete pl;
	}
	_polyLists.clear();
	_lightmap.clear();
}
