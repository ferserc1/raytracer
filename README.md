# C++ platform independent raytracer

## Requirements and dependencies
### Common

This project uses standard C++14 threads and pointers, so you'll need a C++14 compatible compiler.

The executable doesn't use any external dependency, apart from the standard C++14 libraries.

### Windows

A Visual Studio 2017 project is included in the `Visual Studio` directory. The raytracer has been built using the Windows Fall Creators Update 10 SDK (16299), but if you want, you can change it to any Windows 10 SDK in the project settings.

To distribute the executable file, you'll need to supply the Visual Studio 2017 dll's, or install the Visual Studio 2017 redistibutable librarires.

### macOS

In the repository root directory, you'll see a Xcode project titled `raytracer.xcodeproj`. To build the macOS version, you need the Apple Xcode tools, that you can download from the Mac App Store. You probably also need an Apple developer account, but you can get one for free using the same Apple ID that you need to download Xcode from the Mac App Store.

To distribute the application, you only need to copy the binary executable.

### Linux 

Tested in Ubuntu 17.10. You can use apt-get to install dependencies required to build:

```
$ sudo apt-get install cmake
```

Then, create a `build` directory inside the repository (the `build` directory is configured to be ignored by git), change to this directory, and use cmake and make to build the project:

```
$ mkdir build
$ cd build
$ cmake .
$ make
```

You can distribute the application simply copying the binary file.

## Image class example

```c++
// Load image
std::string path = "/path/to/my/image.png";
	
Image * img = Image::Load(path);
if (!img || !img->valid()) {
    std::cerr << "Error opening image file" << std::endl;
    return -1;
}

// Save image
try {
    img->save("/path/to/destination.png");
    img->save("/path/to/destination.jpg");
    img->save("/path/to/destination.tga");
    img->save("/path/to/destination.bmp");
}
catch (std::exception & err) {
    std::cerr << err.what() << std::endl;
    return -2;
}

// Release the image data
img->release(); // do it

// Specify image data
int w = 2000;
int h = 1500;
int bpp = 3;
unsigned char * data = new unsigned char[w * h * bpp];
int stride = w * bpp;
for (int i = 0; i < h; ++i) {
    for (int j = 0; j < w; ++j) {
        data[stride * i + j * bpp] = i * 255 / h;
        data[stride * i + j * bpp + 1] = (h - i) * 255 / h;
        data[stride * i + j * bpp + 2] = j * 255 / w;
        if (bpp > 3) {
            data[stride * i + j * bpp + 3] = 255;
        }
    }
}
img->setData(data, w, h, bpp);

```